module gitlab.com/Bogdanovds/go-kata

go 1.18

require (
	github.com/brianvoe/gofakeit v3.18.0+incompatible
	github.com/brianvoe/gofakeit/v6 v6.20.2
	github.com/essentialkaos/translit v2.0.3+incompatible
	github.com/essentialkaos/translit/v2 v2.0.4
	github.com/go-chi/chi/v5 v5.0.8
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/stretchr/testify v1.8.2
	github.com/unrolled/render v1.6.0
	gopkg.in/loremipsum.v1 v1.1.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20220908164124-27713097b956 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	pkg.re/essentialkaos/check.v1 v1.2.0 // indirect
)
