package main

import (
	"context"
	"fmt"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/handler"
	repo2 "gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/repo"
	service2 "gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/service"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

func getLogger() *log.Logger {
	wrt, err := os.OpenFile("./module4/webserver/swagger/homework/log.log", os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(fmt.Errorf("log file is not open \n %w", err))
	}

	logger := log.New(wrt, "", log.LstdFlags)

	return logger
}

func main() {
	logger := getLogger()
	userService := service2.UserServ{Repository: repo2.NewUserRepo(logger), Logger: logger}
	petService := service2.PetServ{Repository: repo2.NewPetRepo(logger), Logger: logger}
	orderService := service2.OrderServ{Repository: repo2.NewOrderRepo(logger), Logger: logger}

	hndlr := handler.Handler{UserService: &userService, PetService: &petService, OrderService: &orderService}
	routing := hndlr.Routing(logger)

	srv := &http.Server{Addr: ":30009", Handler: routing}

	go func() {
		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			panic(err)
		}

	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancelCnt := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelCnt()
	err := srv.Shutdown(ctx)
	if err != nil {
		panic(err)
	}

}
