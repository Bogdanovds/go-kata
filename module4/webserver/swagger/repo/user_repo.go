package repo

import (
	"fmt"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
	"log"
	"sync"
)

type UserRepo struct {
	UserRepository
	Logger             *log.Logger
	data               []*model.User
	primaryKeyIDx      map[int64]*model.User
	autoIncrementCount int64
	sync.Mutex
}

func NewUserRepo(logger *log.Logger) *UserRepo {
	return &UserRepo{
		data:               make([]*model.User, 0, 13),
		primaryKeyIDx:      make(map[int64]*model.User),
		autoIncrementCount: 1,
		Logger:             logger,
	}
}

func (repo *UserRepo) Create(user model.User) (model.User, error) {
	repo.Lock()
	defer repo.Unlock()
	user.ID = repo.autoIncrementCount
	repo.primaryKeyIDx[user.ID] = &user
	repo.autoIncrementCount++
	repo.data = append(repo.data, &user)

	repo.log("Create %v", user)
	return user, nil
}

func (repo *UserRepo) Update(user model.User) error {
	if _, ok := repo.primaryKeyIDx[user.ID]; !ok {
		return fmt.Errorf("user %d, not found", user.ID)
	}

	repo.primaryKeyIDx[user.ID] = &user

	for i := 0; i < len(repo.data); i++ {
		if repo.data[i].ID == user.ID {
			repo.data[i] = &user
			repo.log("Update %v", user)
			return nil
		}
	}
	return fmt.Errorf("user %d, not found", user.ID)
}

func (repo *UserRepo) Delete(id int64) error {
	delete(repo.primaryKeyIDx, id)

	for i := 0; i < len(repo.data); i++ {
		if repo.data[i].ID == id {
			if i != len(repo.data)-1 {
				repo.data[i] = repo.data[len(repo.data)-1]
			}
			repo.data = repo.data[:len(repo.data)-1]
			repo.log("Delete user %v", id)
			return nil
		}
	}

	return fmt.Errorf("user %d, not found", id)
}

func (repo *UserRepo) GetByID(id int64) (model.User, error) {
	if user, ok := repo.primaryKeyIDx[id]; ok {
		return *user, nil
	}

	return model.User{}, fmt.Errorf("user %d, not found", id)
}

func (repo *UserRepo) GetList() ([]*model.User, error) {
	repo.log("Get all users")
	return repo.data, nil
}

func (repo *UserRepo) log(format string, v ...any) {
	if repo.Logger != nil {
		l := fmt.Sprintf(format, v...)
		repo.Logger.Printf("UserRepo\t %s \n", l)
	}
}
