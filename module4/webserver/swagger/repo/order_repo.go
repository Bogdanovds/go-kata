package repo

import (
	"fmt"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
	"log"
	"sync"
)

type OrderRepo struct {
	OrderRepository
	Logger             *log.Logger
	data               []*model.Order
	primaryKeyIDx      map[int64]*model.Order
	autoIncrementCount int64
	sync.Mutex
}

func NewOrderRepo(logger *log.Logger) *OrderRepo {
	return &OrderRepo{
		data:               make([]*model.Order, 0, 13),
		primaryKeyIDx:      make(map[int64]*model.Order),
		autoIncrementCount: 1,
		Logger:             logger,
	}
}

func (repo *OrderRepo) Create(order model.Order) (model.Order, error) {
	repo.Lock()
	defer repo.Unlock()
	order.ID = repo.autoIncrementCount
	repo.primaryKeyIDx[order.ID] = &order
	repo.autoIncrementCount++
	repo.data = append(repo.data, &order)

	repo.log("Create %v", order)
	return order, nil
}

func (repo *OrderRepo) Update(order model.Order) error {
	if _, ok := repo.primaryKeyIDx[order.ID]; !ok {
		return fmt.Errorf("order %d, not found", order.ID)
	}

	repo.primaryKeyIDx[order.ID] = &order

	for i := 0; i < len(repo.data); i++ {
		if repo.data[i].ID == order.ID {
			repo.data[i] = &order
			repo.log("Update %v", order)
			return nil
		}
	}
	return fmt.Errorf("order %d, not found", order.ID)
}

func (repo *OrderRepo) Delete(id int64) error {
	delete(repo.primaryKeyIDx, id)

	for i := 0; i < len(repo.data); i++ {
		if repo.data[i].ID == id {
			if i != len(repo.data)-1 {
				repo.data[i] = repo.data[len(repo.data)-1]
			}
			repo.data = repo.data[:len(repo.data)-1]
			repo.log("Delete order %v", id)
			return nil
		}
	}

	return fmt.Errorf("order %d, not found", id)
}

func (repo *OrderRepo) GetByID(id int64) (model.Order, error) {
	if order, ok := repo.primaryKeyIDx[id]; ok {
		return *order, nil
	}

	return model.Order{}, fmt.Errorf("order %d, not found", id)
}

func (repo *OrderRepo) GetList() ([]*model.Order, error) {
	repo.log("Get all orders")
	return repo.data, nil
}

func (repo *OrderRepo) log(format string, v ...any) {
	if repo.Logger != nil {
		l := fmt.Sprintf(format, v...)
		repo.Logger.Printf("OrderRepo\t %s \n", l)
	}
}
