package repo

import (
	model2 "gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
)

type UserRepository interface {
	Create(user model2.User) (model2.User, error)
	Update(user model2.User) error
	Delete(id int64) error
	GetByID(id int64) (model2.User, error)
	GetList() ([]*model2.User, error)
}

type PetRepository interface {
	Create(pet model2.Pet) (model2.Pet, error)
	Update(pet model2.Pet) error
	Delete(id int64) error
	GetByID(id int64) (model2.Pet, error)
	GetList() ([]*model2.Pet, error)
}

type OrderRepository interface {
	Create(order model2.Order) (model2.Order, error)
	Update(order model2.Order) error
	Delete(id int64) error
	GetByID(id int64) (model2.Order, error)
	GetList() ([]*model2.Order, error)
}
