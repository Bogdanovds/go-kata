package repo

import (
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
	"log"
	"reflect"
	"testing"
)

func TestNewUserRepo(t *testing.T) {
	type args struct {
		user model.User
	}
	tests := []struct {
		name string
		args args
		want model.User
	}{
		{
			name: "first test",
			args: args{
				user: model.User{
					ID:         1,
					Username:   "Jon",
					FirstName:  "J",
					LastName:   "Johansan",
					Email:      "Johansan@jj.com",
					Password:   "qwerty",
					Phone:      "+79618617744",
					UserStatus: 2,
				},
			},
			want: model.User{
				ID:         1,
				Username:   "Jon",
				FirstName:  "J",
				LastName:   "Johansan",
				Email:      "Johansan@jj.com",
				Password:   "qwerty",
				Phone:      "+79618617744",
				UserStatus: 2,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewUserRepo(nil)
			var got model.User
			got, err := p.Create(tt.args.user)
			if err != nil {
				t.Errorf("Create() error = %v", err)
				return
			}
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestUserRepo_Create(t *testing.T) {
	type fields struct {
		UserRepository     UserRepository
		Logger             *log.Logger
		data               []*model.User
		primaryKeyIDx      map[int64]*model.User
		autoIncrementCount int64
	}
	type args struct {
		user model.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.User
		wantErr bool
	}{
		{
			name: "",
			fields: fields{
				data:               make([]*model.User, 0, 13),
				primaryKeyIDx:      make(map[int64]*model.User),
				autoIncrementCount: 1,
			},
			args: args{
				user: model.User{Username: "Jon"},
			},
			want: model.User{
				ID:       1,
				Username: "Jon",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &UserRepo{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := repo.Create(tt.args.user)
			if (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepo_Delete(t *testing.T) {
	type fields struct {
		UserRepository     UserRepository
		Logger             *log.Logger
		data               []*model.User
		primaryKeyIDx      map[int64]*model.User
		autoIncrementCount int64
	}
	type args struct {
		id int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "is in the base",
			fields: fields{
				data:               []*model.User{{ID: 1, Username: "Jon"}},
				primaryKeyIDx:      map[int64]*model.User{1: {ID: 1, Username: "Jon"}},
				autoIncrementCount: 2,
			},
			args:    args{id: 1},
			wantErr: false},
		{
			name: "is not in base",
			fields: fields{
				data:               []*model.User{{ID: 1, Username: "Jon"}},
				primaryKeyIDx:      map[int64]*model.User{1: {ID: 1, Username: "Jon"}},
				autoIncrementCount: 2,
			},
			args:    args{id: 3},
			wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &UserRepo{
				UserRepository:     tt.fields.UserRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := repo.Delete(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserRepo_GetByID(t *testing.T) {
	type fields struct {
		UserRepository     UserRepository
		Logger             *log.Logger
		data               []*model.User
		primaryKeyIDx      map[int64]*model.User
		autoIncrementCount int64
	}
	type args struct {
		id int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.User
		wantErr bool
	}{
		{
			name: "is in the base",
			fields: fields{
				data:               []*model.User{{ID: 1, Username: "Jon"}},
				primaryKeyIDx:      map[int64]*model.User{1: {ID: 1, Username: "Jon"}},
				autoIncrementCount: 2,
			},
			args: args{
				id: 1},
			want:    model.User{ID: 1, Username: "Jon"},
			wantErr: false,
		},
		{
			name: "is not in the base",
			fields: fields{
				data:               []*model.User{},
				primaryKeyIDx:      map[int64]*model.User{},
				autoIncrementCount: 2,
			},
			args: args{
				id: 3},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &UserRepo{
				UserRepository:     tt.fields.UserRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := repo.GetByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepo_GetList(t *testing.T) {
	type fields struct {
		UserRepository     UserRepository
		Logger             *log.Logger
		data               []*model.User
		primaryKeyIDx      map[int64]*model.User
		autoIncrementCount int64
	}
	tests := []struct {
		name    string
		fields  fields
		want    []*model.User
		wantErr bool
	}{
		{
			name: "",
			fields: fields{
				data:               []*model.User{{ID: 1, Username: "Jon"}},
				primaryKeyIDx:      map[int64]*model.User{1: {ID: 1, Username: "Jon"}},
				autoIncrementCount: 2,
			},
			want:    []*model.User{{ID: 1, Username: "Jon"}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &UserRepo{
				UserRepository:     tt.fields.UserRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := repo.GetList()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepo_Update(t *testing.T) {
	type fields struct {
		UserRepository     UserRepository
		Logger             *log.Logger
		data               []*model.User
		primaryKeyIDx      map[int64]*model.User
		autoIncrementCount int64
	}
	type args struct {
		user model.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "is in the base",
			fields: fields{
				data:               []*model.User{{ID: 1, Username: "Jon"}},
				primaryKeyIDx:      map[int64]*model.User{1: {ID: 1, Username: "Jon"}},
				autoIncrementCount: 2,
			},
			args:    args{user: model.User{ID: 1, Username: "NewJon"}},
			wantErr: false},
		{
			name: "is not in base",
			fields: fields{
				data:               []*model.User{{ID: 1, Username: "Jon"}},
				primaryKeyIDx:      map[int64]*model.User{1: {ID: 1, Username: "Jon"}},
				autoIncrementCount: 2,
			},
			args:    args{user: model.User{ID: 43, Username: "NewJon"}},
			wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &UserRepo{
				UserRepository:     tt.fields.UserRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := repo.Update(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
