package repo

import (
	"fmt"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
	"log"
	"sync"
)

type PetRepo struct {
	PetRepository
	Logger             *log.Logger
	data               []*model.Pet
	primaryKeyIDx      map[int64]*model.Pet
	autoIncrementCount int64
	sync.Mutex
}

func NewPetRepo(logger *log.Logger) *PetRepo {
	return &PetRepo{
		data:               make([]*model.Pet, 0, 13),
		primaryKeyIDx:      make(map[int64]*model.Pet),
		autoIncrementCount: 1,
		Logger:             logger,
	}
}

func (repo *PetRepo) Create(pet model.Pet) (model.Pet, error) {
	repo.Lock()
	defer repo.Unlock()
	pet.ID = repo.autoIncrementCount
	repo.primaryKeyIDx[pet.ID] = &pet
	repo.autoIncrementCount++
	repo.data = append(repo.data, &pet)

	repo.log("Create %v", pet)
	return pet, nil
}

func (repo *PetRepo) Update(pet model.Pet) error {
	if _, ok := repo.primaryKeyIDx[pet.ID]; !ok {
		return fmt.Errorf("pet %d, not found", pet.ID)
	}

	repo.primaryKeyIDx[pet.ID] = &pet

	for i := 0; i < len(repo.data); i++ {
		if repo.data[i].ID == pet.ID {
			repo.data[i] = &pet
			repo.log("Update %v", pet)
			return nil
		}
	}
	return fmt.Errorf("pet %d, not found", pet.ID)
}

func (repo *PetRepo) Delete(id int64) error {
	delete(repo.primaryKeyIDx, id)

	for i := 0; i < len(repo.data); i++ {
		if repo.data[i].ID == id {
			if i != len(repo.data)-1 {
				repo.data[i] = repo.data[len(repo.data)-1]
			}
			repo.data = repo.data[:len(repo.data)-1]
			repo.log("Delete pet %v", id)
			return nil
		}
	}

	return fmt.Errorf("pet %d, not found", id)
}

func (repo *PetRepo) GetByID(id int64) (model.Pet, error) {
	if pet, ok := repo.primaryKeyIDx[id]; ok {
		return *pet, nil
	}

	return model.Pet{}, fmt.Errorf("pet %d, not found", id)
}

func (repo *PetRepo) GetList() ([]*model.Pet, error) {
	repo.log("Get all pets")
	return repo.data, nil
}

func (repo *PetRepo) log(format string, v ...any) {
	if repo.Logger != nil {
		l := fmt.Sprintf(format, v...)
		repo.Logger.Printf("PetRepo\t %s \n", l)
	}
}
