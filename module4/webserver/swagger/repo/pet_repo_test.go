package repo

import (
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
	"log"
	"reflect"
	"testing"
)

func TestNewPetRepo(t *testing.T) {
	type args struct {
		pet model.Pet
	}
	tests := []struct {
		name string
		args args
		want model.Pet
	}{
		{
			name: "first test",
			args: args{
				pet: model.Pet{
					ID: 0,
					Category: model.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []model.Category{},
					Status: "active",
				},
			},
			want: model.Pet{
				ID: 0,
				Category: model.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []model.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewPetRepo(nil)
			var got model.Pet
			got, err := p.Create(tt.args.pet)
			if err != nil {
				t.Errorf("Create() error = %v", err)
				return
			}
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestPetRepo_Create(t *testing.T) {
	type fields struct {
		PetRepository      PetRepository
		Logger             *log.Logger
		data               []*model.Pet
		primaryKeyIDx      map[int64]*model.Pet
		autoIncrementCount int64
	}
	type args struct {
		pet model.Pet
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Pet
		wantErr bool
	}{
		{
			name: "",
			fields: fields{
				data:               make([]*model.Pet, 0, 13),
				primaryKeyIDx:      make(map[int64]*model.Pet),
				autoIncrementCount: 1,
			},
			args: args{
				pet: model.Pet{Name: "Dio"},
			},
			want: model.Pet{
				ID:   1,
				Name: "Dio",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &PetRepo{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := repo.Create(tt.args.pet)
			if (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetRepo_Delete(t *testing.T) {
	type fields struct {
		PetRepository      PetRepository
		Logger             *log.Logger
		data               []*model.Pet
		primaryKeyIDx      map[int64]*model.Pet
		autoIncrementCount int64
	}
	type args struct {
		id int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "is in the base",
			fields: fields{
				data:               []*model.Pet{{ID: 1, Name: "Dodo"}},
				primaryKeyIDx:      map[int64]*model.Pet{1: {ID: 1, Name: "Dodo"}},
				autoIncrementCount: 2,
			},
			args:    args{id: 1},
			wantErr: false},
		{
			name: "is not in base",
			fields: fields{
				data:               []*model.Pet{{ID: 1, Name: "Dodo"}},
				primaryKeyIDx:      map[int64]*model.Pet{1: {ID: 1, Name: "Dodo"}},
				autoIncrementCount: 2,
			},
			args:    args{id: 3},
			wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &PetRepo{
				PetRepository:      tt.fields.PetRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := repo.Delete(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPetRepo_GetByID(t *testing.T) {
	type fields struct {
		PetRepository      PetRepository
		Logger             *log.Logger
		data               []*model.Pet
		primaryKeyIDx      map[int64]*model.Pet
		autoIncrementCount int64
	}
	type args struct {
		id int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Pet
		wantErr bool
	}{
		{
			name: "is in the base",
			fields: fields{
				data:               []*model.Pet{{ID: 1, Name: "Dodo"}},
				primaryKeyIDx:      map[int64]*model.Pet{1: {ID: 1, Name: "Dodo"}},
				autoIncrementCount: 2,
			},
			args: args{
				id: 1},
			want:    model.Pet{ID: 1, Name: "Dodo"},
			wantErr: false,
		},
		{
			name: "is not in the base",
			fields: fields{
				data:               []*model.Pet{},
				primaryKeyIDx:      map[int64]*model.Pet{},
				autoIncrementCount: 2,
			},
			args: args{
				id: 3},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &PetRepo{
				PetRepository:      tt.fields.PetRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := repo.GetByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetRepo_GetList(t *testing.T) {
	type fields struct {
		PetRepository      PetRepository
		Logger             *log.Logger
		data               []*model.Pet
		primaryKeyIDx      map[int64]*model.Pet
		autoIncrementCount int64
	}
	tests := []struct {
		name    string
		fields  fields
		want    []*model.Pet
		wantErr bool
	}{
		{
			name: "",
			fields: fields{
				data:               []*model.Pet{{ID: 1, Name: "Dodo"}},
				primaryKeyIDx:      map[int64]*model.Pet{1: {ID: 1, Name: "Dodo"}},
				autoIncrementCount: 2,
			},
			want:    []*model.Pet{{ID: 1, Name: "Dodo"}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &PetRepo{
				PetRepository:      tt.fields.PetRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := repo.GetList()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetRepo_Update(t *testing.T) {
	type fields struct {
		PetRepository      PetRepository
		Logger             *log.Logger
		data               []*model.Pet
		primaryKeyIDx      map[int64]*model.Pet
		autoIncrementCount int64
	}
	type args struct {
		pet model.Pet
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "is in the base",
			fields: fields{
				data:               []*model.Pet{{ID: 1, Name: "Dodo"}},
				primaryKeyIDx:      map[int64]*model.Pet{1: {ID: 1, Name: "Dodo"}},
				autoIncrementCount: 2,
			},
			args:    args{pet: model.Pet{ID: 1, Name: "Dodo22"}},
			wantErr: false},
		{
			name: "is not in base",
			fields: fields{
				data:               []*model.Pet{{ID: 1, Name: "Dodo"}},
				primaryKeyIDx:      map[int64]*model.Pet{1: {ID: 1, Name: "Dodo"}},
				autoIncrementCount: 2,
			},
			args:    args{pet: model.Pet{ID: 43, Name: "Dodo22"}},
			wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &PetRepo{
				PetRepository:      tt.fields.PetRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := repo.Update(tt.args.pet); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
