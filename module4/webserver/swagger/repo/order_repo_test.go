package repo

import (
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
	"log"
	"reflect"
	"testing"
)

func TestNewOrderRepo(t *testing.T) {
	type args struct {
		order model.Order
	}
	tests := []struct {
		name string
		args args
		want model.Order
	}{
		{
			name: "first test",
			args: args{
				order: model.Order{
					ID:       1,
					PetID:    1,
					Quantity: 3,
					ShipDate: "",
					Status:   "active",
					Complete: false,
				},
			},
			want: model.Order{
				ID:       1,
				PetID:    1,
				Quantity: 3,
				ShipDate: "",
				Status:   "active",
				Complete: false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewOrderRepo(nil)
			var got model.Order
			got, err := p.Create(tt.args.order)
			if err != nil {
				t.Errorf("Create() error = %v", err)
				return
			}
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestOrderRepo_Create(t *testing.T) {
	type fields struct {
		OrderRepository    OrderRepository
		Logger             *log.Logger
		data               []*model.Order
		primaryKeyIDx      map[int64]*model.Order
		autoIncrementCount int64
	}
	type args struct {
		order model.Order
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Order
		wantErr bool
	}{
		{
			name: "",
			fields: fields{
				data:               make([]*model.Order, 0, 13),
				primaryKeyIDx:      make(map[int64]*model.Order),
				autoIncrementCount: 1,
			},
			args: args{
				order: model.Order{Status: "active"},
			},
			want: model.Order{
				ID:     1,
				Status: "active",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &OrderRepo{
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := repo.Create(tt.args.order)
			if (err != nil) != tt.wantErr {
				t.Errorf("Create() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderRepo_Delete(t *testing.T) {
	type fields struct {
		OrderRepository    OrderRepository
		Logger             *log.Logger
		data               []*model.Order
		primaryKeyIDx      map[int64]*model.Order
		autoIncrementCount int64
	}
	type args struct {
		id int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "is in the base",
			fields: fields{
				data:               []*model.Order{{ID: 1, Status: "active"}},
				primaryKeyIDx:      map[int64]*model.Order{1: {ID: 1, Status: "active"}},
				autoIncrementCount: 2,
			},
			args:    args{id: 1},
			wantErr: false},
		{
			name: "is not in base",
			fields: fields{
				data:               []*model.Order{{ID: 1, Status: "active"}},
				primaryKeyIDx:      map[int64]*model.Order{1: {ID: 1, Status: "active"}},
				autoIncrementCount: 2,
			},
			args:    args{id: 3},
			wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &OrderRepo{
				OrderRepository:    tt.fields.OrderRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := repo.Delete(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderRepo_GetByID(t *testing.T) {
	type fields struct {
		OrderRepository    OrderRepository
		Logger             *log.Logger
		data               []*model.Order
		primaryKeyIDx      map[int64]*model.Order
		autoIncrementCount int64
	}
	type args struct {
		id int64
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    model.Order
		wantErr bool
	}{
		{
			name: "is in the base",
			fields: fields{
				data:               []*model.Order{{ID: 1, Status: "active"}},
				primaryKeyIDx:      map[int64]*model.Order{1: {ID: 1, Status: "active"}},
				autoIncrementCount: 2,
			},
			args: args{
				id: 1},
			want:    model.Order{ID: 1, Status: "active"},
			wantErr: false,
		},
		{
			name: "is not in the base",
			fields: fields{
				data:               []*model.Order{},
				primaryKeyIDx:      map[int64]*model.Order{},
				autoIncrementCount: 2,
			},
			args: args{
				id: 3},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &OrderRepo{
				OrderRepository:    tt.fields.OrderRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := repo.GetByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderRepo_GetList(t *testing.T) {
	type fields struct {
		OrderRepository    OrderRepository
		Logger             *log.Logger
		data               []*model.Order
		primaryKeyIDx      map[int64]*model.Order
		autoIncrementCount int64
	}
	tests := []struct {
		name    string
		fields  fields
		want    []*model.Order
		wantErr bool
	}{
		{
			name: "",
			fields: fields{
				data:               []*model.Order{{ID: 1, Status: "active"}},
				primaryKeyIDx:      map[int64]*model.Order{1: {ID: 1, Status: "active"}},
				autoIncrementCount: 2,
			},
			want:    []*model.Order{{ID: 1, Status: "active"}},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &OrderRepo{
				OrderRepository:    tt.fields.OrderRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			got, err := repo.GetList()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderRepo_Update(t *testing.T) {
	type fields struct {
		OrderRepository    OrderRepository
		Logger             *log.Logger
		data               []*model.Order
		primaryKeyIDx      map[int64]*model.Order
		autoIncrementCount int64
	}
	type args struct {
		Order model.Order
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "is in the base",
			fields: fields{
				data:               []*model.Order{{ID: 1, Status: "active"}},
				primaryKeyIDx:      map[int64]*model.Order{1: {ID: 1, Status: "active"}},
				autoIncrementCount: 2,
			},
			args:    args{Order: model.Order{ID: 1, Status: "un active"}},
			wantErr: false},
		{
			name: "is not in base",
			fields: fields{
				data:               []*model.Order{{ID: 1, Status: "active"}},
				primaryKeyIDx:      map[int64]*model.Order{1: {ID: 1, Status: "active"}},
				autoIncrementCount: 2,
			},
			args:    args{Order: model.Order{ID: 43, Status: "un active"}},
			wantErr: true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			repo := &OrderRepo{
				OrderRepository:    tt.fields.OrderRepository,
				Logger:             tt.fields.Logger,
				data:               tt.fields.data,
				primaryKeyIDx:      tt.fields.primaryKeyIDx,
				autoIncrementCount: tt.fields.autoIncrementCount,
			}
			if err := repo.Update(tt.args.Order); (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
