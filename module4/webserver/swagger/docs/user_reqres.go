package docs

import (
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
)

// swagger:route POST /user/createWithArray user getUsers
// Get user by user name
//
// responses:
//  200: getUsersResponses
//  500: description: Internal server error

// swagger:route POST /user/createWithList user getUsers
// Get user by user name
//
// responses:
//  200: getUsersResponses
//  500: description: Internal server error

// swagger:parameters getUsers
//
//nolint:all
type getUsers struct {
	// List of user object
	// required: true
	// in:body
	Body model.User
}

// swagger:response getUsersResponses
//
//nolint:all
type getUsersResponses struct {
	// in:body
	Body []model.User
}

// swagger:route GET /user/{username} user getUser
// Get user by user name
//
// responses:
//  200: getUserResponses
//  404: description: Not found
//  500: description: Internal server error

// swagger:parameters getUser
//
//nolint:all
type getUser struct {
	// ID of user
	// required: true
	// in:path
	Username string `json:"username "`
}

// swagger:response getUserResponses
//
//nolint:all
type getUserResponses struct {
	// in:body
	Body model.User
}

// swagger:route PUT /user user updatedUser
// Updated user
//
// This can only be done by the logged in user.
//
// responses:
//  200: description: "OK"
//  404: description: Not found
//  500: description: Internal server error

// swagger:parameters updatedUser
//
//nolint:all
type updatedUser struct {
	// Create user object
	// required: true
	// in:body
	Body model.User
}

// swagger:route DELETE /user/{id} user deleteUser
// Delete user
//
// This can only be done by the logged in user.
//
// responses:
//  200: description: "OK"
//  404: description: Not found
//  500: description: Internal server error

// swagger:parameters deleteUser
//
//nolint:all
type deleteUser struct {
	// ID of user
	// required: true
	// in:path
	UserID int64 `json:"userID"`
}

// swagger:route POST /user user createUser
// Create user
//
// This can only be done by the logged in user.
//
// responses:
//  200: description: "OK"
//  500: description: Internal server error

// swagger:parameters createUser
//
//nolint:all
type createUser struct {
	// Create user object
	// required: true
	// in:body
	Body model.User
}
