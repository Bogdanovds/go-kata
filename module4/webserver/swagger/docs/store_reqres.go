package docs

import (
	model2 "gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
)

// swagger:route POST /store/order stores createStore
// Place an order for a pet
//
//
// responses:
//  200: description: "OK"
//  500: description: Internal server error

// swagger:parameters createStore
//
//nolint:all
type createStore struct {
	// order placed for purchasing the pet
	// required: true
	// in:body
	Body model2.Pet
}

// swagger:route GET /store/order/{orderId} stores fondStore
// Delete purchase order by ID
//
// For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors
//
// responses:
//  200: fondStoreResponses
//  404: description: Not found
//  500: description: Internal server error

// swagger:parameters fondStore
//
//nolint:all
type fondStore struct {
	// ID of pet that needs to be fetched
	// required: true
	// in:path
	OrderId int64 `json:"orderId"`
}

// swagger:response fondStoreResponses
//
//nolint:all
type fondStoreResponses struct {
	// in:body
	Body model2.Order
}

// swagger:route DELETE /store/order/{orderId} stores deleteStores
// Delete purchase order by ID
//
// For valid response try integer IDs with positive integer value. Negative or non-integer values will generate API errors
//
// responses:
//  200: description: "OK"
//  404: description: Not found

// swagger:parameters deleteStores
//
//nolint:all
type deleteStores struct {
	// ID of the order that needs to be deleted
	// required: true
	// in:path
	OrderId int64 `json:"petID"`
}

// swagger:route GET /store/inventory stores getStores
// Returns pet inventories by status
//
//
// responses:
//  200: getStoresResponses
//  404: description: Not found

// swagger:parameters getStoresResponses
//
//nolint:all
type getStoresResponses struct {
	// in:body
	Body []model2.Order
}
