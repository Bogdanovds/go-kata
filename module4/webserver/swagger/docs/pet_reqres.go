package docs

import (
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
)

// swagger:route POST /pet pet addPet
// Add a new pet to the store
//
// responses:
//  200: description: "OK"
//  500: description: Internal server error

// swagger:parameters addPet
//
//nolint:all
type addPet struct {
	// List of user object
	// required: true
	// in:body
	Body model.Pet
}

// swagger:route PUT /pet pet updatePet
// Update an existing pet
//
// responses:
//  200: description: "OK"
//  404: description: Not found
//  500: description: Internal server error

// swagger:parameters updatePet
//
//nolint:all
type updatePet struct {
	// List of user object
	// required: true
	// in:body
	Body model.Pet
}

// swagger:route GET /pet/findByStatus pet findByStatus
// Find pet by ID
//
// responses:
//  200: findByStatusResponses
//  404: description: Not found

// swagger:parameters findByStatus
//
//nolint:all
type findByStatus struct {
	// ID of pet to return
	// required: true
	// in:query
	Status int `json:"status "`
}

// swagger:response findByStatusResponses
//
//nolint:all
type findByStatusResponses struct {
	// in:body
	Body model.Pet
}

// swagger:route GET /pet/{petId} pet fiendPet
// Find pet by ID
//
// responses:
//  200: fiendPetResponses
//  404: description: Not found

// swagger:parameters fiendPet
//
//nolint:all
type fiendPet struct {
	// ID of pet to return
	// required: true
	// in:path
	PetID int64 `json:"petID"`
}

// swagger:response fiendPetResponses
//
//nolint:all
type fiendPetResponses struct {
	// in:body
	Body model.Pet
}

// swagger:route POST /pet/{petId} pet updatePET
// Updates a pet in the store with form data
//
// responses:
//  200: description: "OK"
//  404: description: Not found

// swagger:parameters updatePET
//
//nolint:all
type updatePET struct {
	// ID of pet that needs to be updated
	// required: true
	// in:path
	PetID int64 `json:"petID"`
}

// swagger:route DELETE /pet/{petId} pet deletePet
// Deletes a pet
//
// responses:
//  200: description: "OK"
//  500: description: Internal server error

// swagger:parameters deletePet
//
//nolint:all
type deletePet struct {
	// Pet id to delete
	// required: true
	// in:path
	PetID int64 `json:"petID"`
}
