package handler

import (
	"io"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func (h *Handler) userRout(r chi.Router) {

	r.Post("/createWithArray", func(w http.ResponseWriter, r *http.Request) {
		body := r.Body
		userB, err := io.ReadAll(body)
		if WriteErr(err, w, 0) {
			return
		}
		defer body.Close()

		users, err := h.UserService.GetUsers(userB)
		if WriteErr(err, w, 0) {
			return
		}

		_, err = w.Write(users)
		if WriteErr(err, w, 0) {
			return
		}

	})

	r.Post("/createWithList", func(w http.ResponseWriter, r *http.Request) {
		body := r.Body
		userB, err := io.ReadAll(body)
		if WriteErr(err, w, 0) {
			return
		}
		defer body.Close()

		users, err := h.UserService.GetUsers(userB)
		if WriteErr(err, w, 0) {
			return
		}

		_, err = w.Write(users)
		if WriteErr(err, w, 0) {
			return
		}

	})

	r.Get("/{username}", func(w http.ResponseWriter, r *http.Request) {
		username := chi.URLParam(r, "username")

		user, err := h.UserService.GetUser(username)
		if WriteErr(err, w, 0) {
			return
		}

		_, err = w.Write(user)
		if WriteErr(err, w, 0) {
			return
		}

	})

	r.Put("/", func(w http.ResponseWriter, r *http.Request) {
		body := r.Body
		userB, err := io.ReadAll(body)
		if WriteErr(err, w, 0) {
			return
		}
		defer body.Close()

		err = h.UserService.UpdateUser(userB)
		if WriteErr(err, w, 404) {
			return
		}

		_, _ = w.Write([]byte("OK"))
	})

	r.Delete("/{id}", func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "id")

		err := h.UserService.DeleteUser(id)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, _ = w.Write([]byte("OK"))
	})

	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		body := r.Body
		userB, err := io.ReadAll(body)
		if WriteErr(err, w, 0) {
			return
		}
		defer body.Close()

		err = h.UserService.AddUser(userB)
		if WriteErr(err, w, 0) {
			return
		}

		_, _ = w.Write([]byte("OK"))

	})

}
