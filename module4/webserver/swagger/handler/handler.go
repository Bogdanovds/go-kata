package handler

import (
	"encoding/json"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/docs"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/service"
	"net/http"

	"github.com/go-chi/chi/v5/middleware"

	"github.com/go-chi/chi/v5"
)

type Handler struct {
	UserService  service.UserService
	PetService   service.PetService
	OrderService service.OrderService
}

func (h *Handler) Routing(logger middleware.LoggerInterface) *chi.Mux {
	httpHandler := middleware.RequestLogger(&middleware.DefaultLogFormatter{Logger: logger, NoColor: true})
	r := chi.NewRouter()

	r.Use(middleware.Logger, httpHandler)

	r.Route("/users", h.userRout)
	r.Route("/store", h.StoreRout)
	r.Route("/pet", h.petRout)

	r.Get("/swagger", docs.SwaggerUI)
	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./module4/webserver/swagger/homework/public"))).ServeHTTP(w, r)
	})

	return r
}

func WriteErr(err error, w http.ResponseWriter, statusCode int) bool {
	if err != nil {
		if statusCode == 0 {
			statusCode = http.StatusInternalServerError
		}
		w.WriteHeader(statusCode)
		marshal, marshalErr := json.Marshal(err)
		if marshalErr != nil {
			_, _ = w.Write([]byte(err.Error()))
		}
		_, _ = w.Write(marshal)
	}
	return err != nil
}
