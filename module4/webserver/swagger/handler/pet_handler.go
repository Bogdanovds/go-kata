package handler

import (
	"io"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func (h *Handler) petRout(r chi.Router) {

	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		body := r.Body
		petB, err := io.ReadAll(body)
		if WriteErr(err, w, 0) {
			return
		}
		defer body.Close()

		err = h.PetService.AddPet(petB)
		if WriteErr(err, w, 0) {
			return
		}

		_, _ = w.Write([]byte("OK"))
	})

	r.Put("/", func(w http.ResponseWriter, r *http.Request) {
		body := r.Body
		petB, err := io.ReadAll(body)
		if WriteErr(err, w, 0) {
			return
		}
		defer body.Close()

		err = h.PetService.UpdatePet(petB)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, _ = w.Write([]byte("OK"))
	})

	r.Get("/findByStatus", func(w http.ResponseWriter, r *http.Request) {
		status := chi.URLParam(r, "status ")

		pets, err := h.PetService.FindPetsByStatus(status)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, err = w.Write(pets)
		if WriteErr(err, w, 0) {
			return
		}
	})

	r.Get("/{petId}", func(w http.ResponseWriter, r *http.Request) {
		petId := chi.URLParam(r, "petId")

		pet, err := h.PetService.GetPet(petId)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, err = w.Write(pet)
		if WriteErr(err, w, 0) {
			return
		}
	})

	r.Post("/{petId}", func(w http.ResponseWriter, r *http.Request) {
		petId := chi.URLParam(r, "petId")
		name := chi.URLParam(r, "name")
		status := chi.URLParam(r, "status")

		err := h.PetService.UpdateNameAndStatus(petId, name, status)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, _ = w.Write([]byte("OK"))
	})

	r.Delete("/{petId}", func(w http.ResponseWriter, r *http.Request) {
		petId := chi.URLParam(r, "petId")

		err := h.PetService.DeletePet(petId)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, _ = w.Write([]byte("OK"))
	})
}
