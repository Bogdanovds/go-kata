package handler

import (
	"io"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func (h *Handler) StoreRout(r chi.Router) {

	r.Post("/order", func(w http.ResponseWriter, r *http.Request) {
		body := r.Body
		orderB, err := io.ReadAll(body)
		if WriteErr(err, w, 0) {
			return
		}
		defer body.Close()

		err = h.OrderService.PlacedOrder(orderB)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, _ = w.Write([]byte("OK"))
	})

	r.Get("/order/{orderId}", func(w http.ResponseWriter, r *http.Request) {
		orderId := chi.URLParam(r, "orderId")

		order, err := h.OrderService.GetOrder(orderId)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, err = w.Write(order)
		if WriteErr(err, w, 0) {
			return
		}
	})

	r.Delete("/order/{orderId}", func(w http.ResponseWriter, r *http.Request) {
		orderId := chi.URLParam(r, "orderId")

		err := h.OrderService.DeleteOrder(orderId)
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, _ = w.Write([]byte("OK"))

	})

	r.Get("/inventory", func(w http.ResponseWriter, r *http.Request) {
		orders, err := h.OrderService.GetOrders()
		if WriteErr(err, w, http.StatusNotFound) {
			return
		}

		_, err = w.Write(orders)
		if WriteErr(err, w, 0) {
			return
		}

	})

}
