package service

import (
	"encoding/json"
	"fmt"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/repo"
	"log"
	"strconv"
)

type UserServ struct {
	Logger     *log.Logger
	Repository repo.UserRepository
}

func (serv *UserServ) GetUser(name string) ([]byte, error) {
	var err error
	users, err := serv.Repository.GetList()
	if err != nil {
		return nil, err
	}
	for _, user := range users {
		if user.Username == name {
			userJSON, err := json.Marshal(user)
			if err != nil {
				return nil, err
			}
			serv.log("GetUser")
			return userJSON, err
		}
	}
	return nil, fmt.Errorf("user %s nit found", name)
}

func (serv *UserServ) UpdateUser(userB []byte) error {
	var (
		user model.User
		err  error
	)
	err = json.Unmarshal(userB, &user)
	if err != nil {
		return err
	}

	err = serv.Repository.Update(user)
	if err != nil {
		return err
	}

	serv.log("UpdateUser")
	return nil
}

func (serv *UserServ) DeleteUser(idS string) error {
	var err error
	id, err := strconv.ParseInt(idS, 0, 64)
	if err != nil {
		return err
	}

	err = serv.Repository.Delete(id)
	if err != nil {
		return err
	}

	serv.log("DeleteUser")
	return nil
}

func (serv *UserServ) GetUsers(userB []byte) ([]byte, error) {
	var (
		user model.User
		err  error
	)
	err = json.Unmarshal(userB, &user)
	if err != nil {
		return nil, err
	}

	users, err := serv.Repository.GetList()
	if err != nil {
		return nil, err
	}

	ret := make([]model.User, 0, len(users)/10)

	for i := 0; i < len(users); i++ {
		if len(user.Username) > 0 && user.Username != users[i].Username {
			continue
		}
		if len(user.Email) > 0 && user.Email != users[i].Email {
			continue
		}
		if len(user.FirstName) > 0 && user.FirstName != users[i].FirstName {
			continue
		}
		if len(user.LastName) > 0 && user.LastName != users[i].LastName {
			continue
		}
		if len(user.Phone) > 0 && user.Phone != users[i].Phone {
			continue
		}
		ret = append(ret, *users[i])
	}

	retJSON, err := json.Marshal(ret)
	if err != nil {
		return nil, err
	}

	serv.log("GetUsers")
	return retJSON, nil
}

func (serv *UserServ) AddUser(userB []byte) error {
	var (
		user model.User
		err  error
	)

	err = json.Unmarshal(userB, &user)
	if err != nil {
		return err
	}

	_, err = serv.Repository.Create(user)
	if err != nil {
		return err
	}

	serv.log("AddUser")
	return nil
}

func (serv *UserServ) log(format string, v ...any) {
	if serv.Logger != nil {
		l := fmt.Sprintf(format, v...)
		serv.Logger.Printf("UserRepo\t %s \n", l)
	}
}
