package service

type UserService interface {
	GetUser(name string) ([]byte, error)
	UpdateUser(userB []byte) error
	DeleteUser(idS string) error
	GetUsers(userB []byte) ([]byte, error)
	AddUser(userB []byte) error
}

type PetService interface {
	AddPet(petB []byte) error
	UpdatePet(petB []byte) error
	FindPetsByStatus(status string) ([]byte, error)
	GetPet(idS string) ([]byte, error)
	DeletePet(idS string) error
	UpdateNameAndStatus(idS, name, status string) error
}

type OrderService interface {
	AddOrder(orderB []byte) error
	GetOrder(idS string) ([]byte, error)
	DeleteOrder(idS string) error
	GetOrders() ([]byte, error)
	PlacedOrder(orderB []byte) error
}
