package service

import (
	"encoding/json"
	"fmt"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/repo"
	"log"
	"strconv"
)

type PetServ struct {
	Logger     *log.Logger
	Repository repo.PetRepository
}

func (serv *PetServ) AddPet(petB []byte) error {
	var (
		pet model.Pet
		err error
	)

	err = json.Unmarshal(petB, &pet)
	if err != nil {
		return err
	}

	_, err = serv.Repository.Create(pet)
	if err != nil {
		return err
	}

	serv.log("AddPet")
	return nil
}

func (serv *PetServ) UpdatePet(petB []byte) error {
	var (
		pet model.Pet
		err error
	)

	err = json.Unmarshal(petB, &pet)
	if err != nil {
		return err
	}

	err = serv.Repository.Update(pet)
	if err != nil {
		return err
	}

	serv.log("UpdatePet")
	return nil
}

func (serv *PetServ) FindPetsByStatus(status string) ([]byte, error) {
	var err error
	allPets, err := serv.Repository.GetList()
	if err != nil {
		return nil, err
	}

	ret := make([]model.Pet, 0, len(allPets)/10)
	for _, pet := range allPets {
		if pet.Status == status {
			ret = append(ret, *pet)
		}
	}

	retJson, err := json.Marshal(ret)
	if err != nil {
		return nil, err
	}

	serv.log("FindPetsByStatus")
	return retJson, nil
}

func (serv *PetServ) GetPet(idS string) ([]byte, error) {
	var err error
	id, err := strconv.ParseInt(idS, 0, 64)
	if err != nil {
		return nil, err
	}

	user, err := serv.Repository.GetByID(id)
	if err != nil {
		return nil, err
	}

	userJSON, err := json.Marshal(&user)
	if err != nil {
		return nil, err
	}

	serv.log("GetPet")
	return userJSON, nil
}

func (serv *PetServ) DeletePet(idS string) error {
	var err error
	id, err := strconv.ParseInt(idS, 0, 64)
	if err != nil {
		return err
	}

	err = serv.Repository.Delete(id)
	if err != nil {
		return err
	}

	serv.log("DeletePet")
	return nil
}

func (serv *PetServ) UpdateNameAndStatus(idS, name, status string) error {
	var (
		pet model.Pet
		err error
	)

	id, err := strconv.ParseInt(idS, 0, 64)
	if err != nil {
		return err
	}

	pet, err = serv.Repository.GetByID(id)
	if err != nil {
		return err
	}

	if len(name) > 0 {
		pet.Name = name
	}

	if len(status) > 0 {
		pet.Status = status
	}

	err = serv.Repository.Update(pet)
	if err != nil {
		return err
	}

	serv.log("UpdateNameAndStatus")
	return nil
}

func (serv *PetServ) log(format string, v ...any) {
	if serv.Logger != nil {
		l := fmt.Sprintf(format, v...)
		serv.Logger.Printf("UserRepo\t %s \n", l)
	}
}
