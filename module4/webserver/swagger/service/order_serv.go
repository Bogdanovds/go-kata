package service

import (
	"encoding/json"
	"fmt"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/model"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/swagger/repo"
	"log"
	"strconv"
)

type OrderServ struct {
	Logger     *log.Logger
	Repository repo.OrderRepository
}

func (serv *OrderServ) AddOrder(orderB []byte) error {
	var (
		order model.Order
		err   error
	)

	err = json.Unmarshal(orderB, &order)
	if err != nil {
		return err
	}

	_, err = serv.Repository.Create(order)
	if err != nil {
		return err
	}

	serv.log("AddOrder")
	return nil
}

func (serv *OrderServ) GetOrder(idS string) ([]byte, error) {
	var err error
	id, err := strconv.ParseInt(idS, 0, 64)
	if err != nil {
		return nil, err
	}

	order, err := serv.Repository.GetByID(id)
	if err != nil {
		return nil, err
	}

	orderJSON, err := json.Marshal(&order)
	if err != nil {
		return nil, err
	}

	serv.log("GetOrder")
	return orderJSON, nil
}

func (serv *OrderServ) DeleteOrder(idS string) error {
	var err error
	id, err := strconv.ParseInt(idS, 0, 64)
	if err != nil {
		return err
	}

	err = serv.Repository.Delete(id)
	if err != nil {
		return err
	}

	serv.log("DeleteOrder")
	return nil
}

func (serv *OrderServ) GetOrders() ([]byte, error) {
	list, err := serv.Repository.GetList()
	if err != nil {
		return nil, err
	}

	retJson, err := json.Marshal(&list)
	if err != nil {
		return nil, err
	}

	serv.log("GetOrders")
	return retJson, nil
}

func (serv *OrderServ) PlacedOrder(orderB []byte) error {
	var (
		order model.Order
		err   error
	)

	err = json.Unmarshal(orderB, &order)
	if err != nil {
		return err
	}

	order.Status = "order"

	err = serv.Repository.Update(order)
	if err != nil {
		return err
	}

	serv.log("PlacedOrder")
	return nil
}

func (serv *OrderServ) log(format string, v ...any) {
	if serv.Logger != nil {
		l := fmt.Sprintf(format, v...)
		serv.Logger.Printf("OrderServ\t %s \n", l)
	}
}
