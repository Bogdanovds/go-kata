package handler

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/http/homework/service"
	"io"
	"net/http"
	"os"
)

type Handler struct {
	UserService service.Service
}

func (h *Handler) Routing(logger middleware.LoggerInterface) *chi.Mux {
	httpHandler := middleware.RequestLogger(&middleware.DefaultLogFormatter{Logger: logger, NoColor: true})
	r := chi.NewRouter()

	r.Use(middleware.Logger, httpHandler)

	//'/' — отображает приветственное сообщение в формате JSON;
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		j, err := json.Marshal("Hello")
		if err != nil {
			WriteErr(err, w, 0)
		}
		_, err = w.Write(j)
		if err != nil {
			WriteErr(err, w, 0)
		}
	})

	//'/users' — отображает список пользователей в формате JSON;
	r.Route("/users", h.userRout)

	//'/public' — загружает любой файл с сохранением его названия в папку public;
	r.Post("/public", func(w http.ResponseWriter, r *http.Request) {
		if r.MultipartForm == nil {
			err := r.ParseMultipartForm(32 << 20)
			if err != nil {
				WriteErr(err, w, 0)
			}
		}

		if r.MultipartForm != nil && r.MultipartForm.File != nil {
			for fhs := range r.MultipartForm.File {
				mulFile, header, err := r.FormFile(fhs)
				if err != nil {
					WriteErr(err, w, 0)
				}

				file, err := os.Create(fmt.Sprintf("./module4/webserver/http/homework/public/%s", header.Filename))
				if err != nil {
					WriteErr(err, w, 0)
				}

				_, err = io.Copy(file, mulFile)
				if err != nil {
					WriteErr(err, w, 0)
				}

				err = mulFile.Close()
				if err != nil {
					WriteErr(err, w, 0)
				}
				err = file.Close()
				if err != nil {
					WriteErr(err, w, 0)
				}
			}
		}

	})

	//'/public/filename.txt' — получение файлов из папки public.
	r.Get("/public/{filename}", func(w http.ResponseWriter, r *http.Request) {
		filename := chi.URLParam(r, "filename")

		file, err := os.Open(fmt.Sprintf("./module4/webserver/http/homework/public/%s", filename))
		if err != nil {
			WriteErr(err, w, 0)
		}

		_, err = io.Copy(w, file)
		if err != nil {
			WriteErr(err, w, 0)
		}

		err = file.Close()
		if err != nil {
			WriteErr(err, w, 0)
		}
	})

	return r
}

func WriteErr(err error, w http.ResponseWriter, statusCode int) bool {
	if err != nil {
		if statusCode == 0 {
			statusCode = http.StatusInternalServerError
		}
		w.WriteHeader(statusCode)
		marshal, marshalErr := json.Marshal(err)
		if marshalErr != nil {
			_, _ = w.Write([]byte(err.Error()))
		}
		_, _ = w.Write(marshal)
	}
	return err != nil
}
