package handler

import (
	"net/http"

	"github.com/go-chi/chi/v5"
)

func (h *Handler) userRout(r chi.Router) {

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		users, err := h.UserService.GetUsers()
		if err != nil {
			WriteErr(err, w, 0)
		}
		_, err = w.Write(users)
		if err != nil {
			WriteErr(err, w, 0)
		}
	})

	//'/users/:id' — отображает информацию о конкретном пользователе в формате JSON;
	r.Get("/{ID}", func(w http.ResponseWriter, r *http.Request) {
		id := chi.URLParam(r, "ID")
		user, err := h.UserService.GetUser(id)
		if err != nil {
			WriteErr(err, w, 0)
		}
		_, err = w.Write(user)
		if err != nil {
			WriteErr(err, w, 0)
		}
	})

	//Реализуй обработку get- и post-запросов для маршрута '/users'.
	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		err := h.UserService.AddUser(r.Body)
		if err != nil {
			WriteErr(err, w, 0)
		}
	})

}
