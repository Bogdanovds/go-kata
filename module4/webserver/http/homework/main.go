package main

import (
	"context"
	"fmt"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/http/homework/handler"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/Bogdanovds/go-kata/module4/webserver/http/homework/model"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/http/homework/repo"
	"gitlab.com/Bogdanovds/go-kata/module4/webserver/http/homework/service"
)

func initLogger() *log.Logger {
	wrt, err := os.OpenFile("logFile", os.O_RDONLY|os.O_CREATE, 0666)
	if err != nil {
		panic(fmt.Errorf("log file is not open \n %w", err))
	}

	//Реализуй механизм логирования запросов и ответов.
	logger := log.New(wrt, "", log.LstdFlags)

	return logger
}

func initUserServise(logger *log.Logger) service.Service {
	return service.UserServ{Loger: logger, Repository: &repo.UserRepo{Loger: logger, Users: []model.User{}}}
}

func main() {
	logger := initLogger()
	userServ := initUserServise(logger)
	r := handler.Handler{UserService: userServ}

	h := r.Routing(logger)

	srv := &http.Server{Addr: ":8000", Handler: h}

	//Реализуй механизм graceful shutdown для корректной остановки сервера.
	go func() {
		err := srv.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			panic(err)
		}

	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit

	ctx, cancelCnt := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelCnt()
	err := srv.Shutdown(ctx)
	if err != nil {
		panic(err)
	}

}
