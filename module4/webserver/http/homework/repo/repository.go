package repo

import "gitlab.com/Bogdanovds/go-kata/module4/webserver/http/homework/model"

type Repository interface {
	GetUsers() ([]model.User, error)
	GetUser(id uint64) (model.User, error)
	AddUser(user model.User) error
}
