package repo

import (
	"fmt"
	"log"

	"gitlab.com/Bogdanovds/go-kata/module4/webserver/http/homework/model"
)

type UserRepo struct {
	Loger *log.Logger
	Users []model.User
}

func (repo *UserRepo) GetUsers() ([]model.User, error) {
	repo.log("Get Users")
	return repo.Users, nil
}

func (repo *UserRepo) GetUser(id uint64) (model.User, error) {
	for _, user := range repo.Users {
		if user.Id == id {
			repo.log("Get %v\n", user)
		}

	}
	err := fmt.Errorf("User with %d not found", id)
	repo.log("%v", err)
	return model.User{}, err
}

func (repo *UserRepo) AddUser(user model.User) error {
	repo.Users = append(repo.Users, user)
	return nil
}

func (repo *UserRepo) log(format string, v ...any) {
	l := fmt.Sprintf(format, v...)
	repo.Loger.Printf("UserRepo\t %s \n", l)
}
