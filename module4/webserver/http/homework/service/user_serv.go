package service

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"strconv"

	"gitlab.com/Bogdanovds/go-kata/module4/webserver/http/homework/model"

	"gitlab.com/Bogdanovds/go-kata/module4/webserver/http/homework/repo"
)

type UserServ struct {
	Loger      *log.Logger
	Repository repo.Repository
}

func (serv UserServ) GetUsers() ([]byte, error) {
	users, err := serv.Repository.GetUsers()
	if err != nil {
		serv.log("Error when calling GetUsers \n %v", err)
		return nil, err
	}
	marshal, err := json.Marshal(users)
	if err != nil {
		serv.log("Marshal error with %v \n %v", users, err)
		return nil, err
	}
	return marshal, nil
}

func (serv UserServ) GetUser(idS string) ([]byte, error) {
	id, err := strconv.ParseUint(idS, 10, 0)
	if err != nil {
		serv.log("Error parse %s to uint \n %v", idS, err)
		return nil, err
	}
	user, err := serv.Repository.GetUser(id)
	if err != nil {
		serv.log("Error when calling GetUser \n %v", err)
		return nil, err
	}
	marshal, err := json.Marshal(user)
	if err != nil {
		serv.log("Marshal error with %v \n %v", user, err)
		return nil, err
	}
	return marshal, nil
}

func (serv UserServ) AddUser(userIO io.ReadCloser) error {
	var user model.User

	err := json.NewDecoder(userIO).Decode(&user)
	if err != nil {
		serv.log("Failed unmarshal of %v \n %v", userIO, err)
		return err
	}

	err = serv.Repository.AddUser(user)
	if err != nil {
		serv.log("Failed AddUser of %v \n %v", user, err)
		return err
	}

	return nil
}

func (serv UserServ) log(format string, v ...any) {
	l := fmt.Sprintf(format, v...)
	serv.Loger.Printf("UserServ\t %s \n", l)
}
