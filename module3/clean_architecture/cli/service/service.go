package serv

import (
	"fmt"

	"gitlab.com/Bogdanovds/go-kata/module3/clean_architecture/cli/repository"
)

type TaskService interface {
	CreateTask(title, description string, id int) (*repository.Task, error)
	UpdateTask(id int, title, description, status string) (*repository.Task, error)
	DeleteTask(id int) error
	GetAllTasks(filter *repository.TaskFilter) ([]*repository.Task, error)
	CompleteTask(id int) (*repository.Task, error)
}

type taskService struct {
	repository repository.TaskRepository
}

func NewTaskService(repo repository.TaskRepository) TaskService {
	return &taskService{repository: repo}
}

func (s *taskService) CreateTask(title, description string, id int) (*repository.Task, error) {
	task := &repository.Task{
		ID:          id,
		Title:       title,
		Description: description,
		Status:      "новая задача",
	}

	if err := s.repository.Create(task); err != nil {
		return nil, fmt.Errorf("ошибка при создании задачи: %w", err)
	}

	return task, nil
}

func (s *taskService) UpdateTask(id int, title, description, status string) (*repository.Task, error) {
	task, err := s.repository.GetByID(id)
	if err != nil {
		return nil, fmt.Errorf("ошибка при получении задачи: %w", err)
	}

	task.Title = title
	task.Description = description
	task.Status = status

	if err := s.repository.Update(task); err != nil {
		return nil, fmt.Errorf("ошибка при обновлении задачи: %w", err)
	}

	return task, nil
}

func (s *taskService) DeleteTask(id int) error {
	err := s.repository.Delete(id)
	if err != nil {
		return fmt.Errorf("ошибка при удалении задачи: %w", err)
	}
	return nil
}

func (s *taskService) GetAllTasks(filter *repository.TaskFilter) ([]*repository.Task, error) {
	tasks, err := s.repository.GetAll(filter)
	if err != nil {
		return nil, fmt.Errorf("ошибка при получении списка задач: %w", err)
	}
	return tasks, nil
}

func (s *taskService) CompleteTask(id int) (*repository.Task, error) {

	task, err := s.repository.GetByID(id)
	if err != nil {
		return nil, fmt.Errorf("ошибка при получении задачи: %w", err)
	}

	task.Status = "задача завершена"
	if err := s.repository.Update(task); err != nil {
		return nil, fmt.Errorf("ошибка при обновлении задачи: %w", err)
	}

	return task, nil
}
