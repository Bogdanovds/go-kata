package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"

	"gitlab.com/Bogdanovds/go-kata/module3/clean_architecture/cli/repository"
	serv "gitlab.com/Bogdanovds/go-kata/module3/clean_architecture/cli/service"
)

type PresentationLayer struct {
	todoService serv.TaskService
}

func NewPresentationLayer(repository repository.TaskRepository) *PresentationLayer {
	return &PresentationLayer{todoService: serv.NewTaskService(repository)}
}

func (p *PresentationLayer) ShowMenu() {
	fmt.Println("1. Показать список задач")
	fmt.Println("2. Добавить задачу")
	fmt.Println("3. Удалить задачу")
	fmt.Println("4. Редактировать задачу по ID")
	fmt.Println("5. Завершить задачу")
	fmt.Println("6. Exit")
}

func (p *PresentationLayer) HandleMenuChoice(choice string) bool {
	switch choice {
	case "1":
		p.ShowTodoList()
	case "2":
		p.AddTodo()
	case "3":
		p.DeleteTodo()
	case "4":
		p.EditTodo()
	case "5":
		p.CompleteTodo()
	case "6":
		return false
	default:
		fmt.Println("Некорректный выбор, попробуйте еще раз.")
	}
	return true
}

func (p *PresentationLayer) ShowTodoList() {
	todos, _ := p.todoService.GetAllTasks(nil)
	if len(todos) == 0 {
		fmt.Println("Список задач пуст.")
	} else {
		for _, todo := range todos {
			fmt.Printf("id:%d %s - %s: %s\n", todo.ID, todo.Title, todo.Description, todo.Status)
		}
	}
}

func (p *PresentationLayer) AddTodo() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Введите заголовок задачи: ")
	scanner.Scan()
	title := scanner.Text()
	fmt.Print("Введите описание задачи: ")
	scanner.Scan()
	description := scanner.Text()
	fmt.Print("Введите id задачи: ")
	scanner.Scan()
	idStr := scanner.Text()
	id, _ := strconv.Atoi(idStr)
	p.todoService.CreateTask(title, description, id)
}

func (p *PresentationLayer) DeleteTodo() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Введите ID задачи: ")
	scanner.Scan()
	todoIDStr := scanner.Text()
	todoID, _ := strconv.Atoi(todoIDStr)
	p.todoService.DeleteTask(todoID)
}

func (p *PresentationLayer) EditTodo() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Введите ID задачи: ")
	scanner.Scan()
	idStr := scanner.Text()
	id, _ := strconv.Atoi(idStr)
	fmt.Print("Введите новый заголовок задачи: ")
	scanner.Scan()
	title := scanner.Text()
	fmt.Print("Введите новое описание задачи: ")
	scanner.Scan()
	description := scanner.Text()
	fmt.Print("Введите новый статус задачи: ")
	scanner.Scan()
	status := scanner.Text()
	p.todoService.UpdateTask(id, title, description, status)
}

func (p *PresentationLayer) CompleteTodo() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Введите ID задачи: ")
	scanner.Scan()
	idStr := scanner.Text()
	id, _ := strconv.Atoi(idStr)
	p.todoService.CompleteTask(id)
}

func main() {
	presentationLayer := NewPresentationLayer(repository.NewTaskRepository())
	for {
		presentationLayer.ShowMenu()
		scanner := bufio.NewScanner(os.Stdin)
		fmt.Print("Выберите действие: ")
		scanner.Scan()
		choice := scanner.Text()
		if !presentationLayer.HandleMenuChoice(choice) {
			break
		}
	}
}
