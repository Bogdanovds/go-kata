package repository

import (
	"bytes"
	"testing"
)

func TestFind(t *testing.T) {
	file := new(bytes.Buffer)
	repo := NewUserRepository(file)

	users := []User{
		{ID: 1, Name: "w"},
		{ID: 2, Name: "q"},
		{ID: 3, Name: "e"},
	}

	if err := repo.Save(users); err != nil {
		t.Errorf("Save returned an error: %v", err)
	}

	result, err := repo.Find(1)
	if err != nil {
		t.Errorf("Find returned an error: %v", err)
	}

	foundUser, ok := result.(User)
	if !ok {
		t.Errorf("Expected result to be of type User, got %v", result)
	}
	if foundUser.ID != users[0].ID || foundUser.Name != users[0].Name {
		t.Errorf("Expected the found user to be %v, got %v", users[0], foundUser)
	}

	_, err = repo.Find(4)
	if err == nil {
		t.Errorf("Expected NotFound")
	}
}

func TestUserRepository_FindAll(t *testing.T) {
	file := new(bytes.Buffer)
	repo := NewUserRepository(file)

	users := []User{
		{ID: 1, Name: "w"},
		{ID: 2, Name: "q"},
		{ID: 3, Name: "e"},
	}

	if err := repo.Save(users); err != nil {
		t.Errorf("Save returned an error: %v", err)
	}

	records, err := repo.FindAll()
	if records == nil || err != nil {
		t.Errorf("Expected FindAll() to return non-nil records and nil error, but got records=%v, err=%v", records, err)
	}
}
