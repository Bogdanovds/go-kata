package repository

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File io.ReadWriter
}

func NewUserRepository(file io.ReadWriter) *UserRepository {
	return &UserRepository{
		File: file,
	}
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

func (r *UserRepository) Save(record interface{}) error {
	jsonData, err := json.Marshal(record)
	if err != nil {
		return err
	}

	if _, err := r.File.Write(jsonData); err != nil {
		return err
	}

	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	buf := new(bytes.Buffer)
	if _, err := io.Copy(buf, r.File); err != nil {
		return User{}, err
	}

	var users []User
	if err := json.Unmarshal(buf.Bytes(), &users); err != nil {
		return User{}, err
	}

	for _, user := range users {
		if user.ID == id {
			return user, nil

		}
	}

	return User{}, fmt.Errorf("NotFound")
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	buf := new(bytes.Buffer)
	if _, err := io.Copy(buf, r.File); err != nil {
		return nil, err
	}

	var users []User
	if err := json.Unmarshal(buf.Bytes(), &users); err != nil {
		return nil, err
	}

	records := make([]interface{}, len(users))
	for i := range users {
		records[i] = &users[i]
	}

	return records, nil
}
