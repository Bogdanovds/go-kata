package test

import (
	"context"
	"testing"

	"gitlab.com/Bogdanovds/go-kata/module3/clean_architecture/service/repo"
	"gitlab.com/Bogdanovds/go-kata/module3/clean_architecture/service/service"
)

func TestCreateTask(t *testing.T) {
	repo := repo.NewTaskRepository()
	service := service.NewTaskService(repo)

	title := "Task1"
	description := "Description1"
	id := 1

	task, err := service.CreateTask(context.Background(), title, description, id)
	if err != nil {
		t.Errorf("ошибка при создании задачи: %v", err)
	}

	if task.Title != title {
		t.Errorf("ошибка при создании задачи title: %v", err)
	}
	if task.Description != description {
		t.Errorf("ошибка при создании задачи description: %v", err)
	}
	if task.ID != id {
		t.Errorf("ошибка при создании задачи id: %v", err)
	}

	if task.Status != "новая задача" {
		t.Errorf("ошибка при создании задачи Status: %v", err)
	}
}

func TestGetTaskByID(t *testing.T) {
	repo := repo.NewTaskRepository()
	service := service.NewTaskService(repo)

	title := "Task1"
	description := "Description1"
	id := 1

	_, err := service.CreateTask(context.Background(), title, description, id)
	if err != nil {
		t.Errorf("ошибка при создании задачи: %v", err)
	}

	task, err := service.GetTaskByID(context.Background(), id)
	if err != nil {
		t.Errorf("ошибка при получении задачи: %v", err)
	}

	if task.Title != title {
		t.Errorf("ошибка при создании задачи title: %v", task.Title)
	}
	if task.Description != description {
		t.Errorf("ошибка при создании задачи description: %v", task.Description)
	}
	if task.ID != id {
		t.Errorf("ошибка при создании задачи id: %v", task.ID)
	}

	if task.Status != "новая задача" {
		t.Errorf("ошибка при создании задачи Status: %v", task.Status)
	}

	task, err = service.GetTaskByID(context.Background(), 2)
	if err == nil {
		t.Errorf("ошибка при получении по несуществующему id: %v", task)
	}
}

func TestUpdateTask(t *testing.T) {
	repo := repo.NewTaskRepository()
	service := service.NewTaskService(repo)

	title := "Task1"
	description := "Description1"
	id := 1

	_, err := service.CreateTask(context.Background(), title, description, id)
	if err != nil {
		t.Errorf("ошибка при создании задачи: %v", err)
	}

	title2 := "Task2"
	description2 := "Description2"
	status2 := "Complete"

	newTask, err := service.UpdateTask(context.Background(), id, title2, description2, status2)
	if err != nil {
		t.Errorf("ошибка при обновлении задачи: %v", err)
	}

	if newTask.Title != title2 {
		t.Errorf("ошибка при создании задачи title: %v", newTask.Title)
	}
	if newTask.Description != description2 {
		t.Errorf("ошибка при создании задачи description: %v", newTask.Description)
	}

	if newTask.Status != status2 {
		t.Errorf("ошибка при создании задачи Status: %v", newTask.Status)
	}

}

func TestDeleteTask(t *testing.T) {
	repo := repo.NewTaskRepository()
	service := service.NewTaskService(repo)

	title := "Task1"
	description := "Description1"
	id := 1

	_, err := service.CreateTask(context.Background(), title, description, id)
	if err != nil {
		t.Errorf("ошибка при создании задачи: %v", err)
	}

	err = service.DeleteTask(context.Background(), id)
	if err != nil {
		t.Errorf("ошибка при удалении задачи %v", err)
		return
	}

	err = service.DeleteTask(context.Background(), 2)
	if err == nil {
		t.Errorf("ошибка при удалении по несуществующему id %v", err)
		return
	}
}

func TestAllTasks(t *testing.T) {

	repos := repo.NewTaskRepository()
	service := service.NewTaskService(repos)

	title := "Task1"
	description := "Description1"
	id := 1

	title2 := "Task2"
	description2 := "Description2"
	id2 := 2

	_, err := service.CreateTask(context.Background(), title, description, id)
	if err != nil {
		t.Errorf("ошибка при создании задачи: %v", err)
	}
	_, err = service.CreateTask(context.Background(), title2, description2, id2)
	if err != nil {
		t.Errorf("ошибка при создании задачи: %v", err)
	}

	tasks, err := service.GetAllTasks(context.Background(), nil)
	if err != nil || len(tasks) != 2 {
		t.Errorf("ошибка при получении всех задач: %v", err)
	}

	title3 := "Task2"
	description3 := "Description2"
	status3 := "Complete"

	_, err = service.UpdateTask(context.Background(), id, title3, description3, status3)
	if err != nil {
		t.Errorf("ошибка при обновлении задачи: %v", err)
	}
	filter := &repo.TaskFilter{Status: status3}
	tasks, err = service.GetAllTasks(context.Background(), filter)
	if err != nil || len(tasks) != 1 {
		t.Errorf("ошибка при получении отфильтрованных задач: %v", err)
	}

}
