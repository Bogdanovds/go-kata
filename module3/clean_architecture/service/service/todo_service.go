package service

import (
	"context"
	"fmt"

	"gitlab.com/Bogdanovds/go-kata/module3/clean_architecture/service/repo"
)

type TaskService interface {
	CreateTask(ctx context.Context, title, description string, id int) (*repo.Task, error)
	UpdateTask(ctx context.Context, id int, title, description, status string) (*repo.Task, error)
	DeleteTask(ctx context.Context, id int) error
	GetAllTasks(ctx context.Context, filter *repo.TaskFilter) ([]*repo.Task, error)
	GetTaskByID(ctx context.Context, id int) (*repo.Task, error)
}

type taskService struct {
	repo repo.TaskRepository
}

func NewTaskService(repo repo.TaskRepository) TaskService {
	return &taskService{repo: repo}
}

func (s *taskService) CreateTask(ctx context.Context, title, description string, id int) (*repo.Task, error) {
	task := &repo.Task{
		ID:          id,
		Title:       title,
		Description: description,
		Status:      "новая задача",
	}

	if err := s.repo.Create(ctx, task); err != nil {
		return nil, fmt.Errorf("ошибка при создании задачи: %w", err)
	}

	return task, nil
}

func (s *taskService) UpdateTask(ctx context.Context, id int, title, description, status string) (*repo.Task, error) {
	task, err := s.repo.GetByID(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("ошибка при получении задачи: %w", err)
	}

	task.Title = title
	task.Description = description
	task.Status = status

	if err := s.repo.Update(ctx, task); err != nil {
		return nil, fmt.Errorf("ошибка при обновлении задачи: %w", err)
	}

	return task, nil
}

func (s *taskService) DeleteTask(ctx context.Context, id int) error {
	err := s.repo.Delete(ctx, id)
	if err != nil {
		return fmt.Errorf("ошибка при удалении задачи: %w", err)
	}
	return nil
}

func (s *taskService) GetAllTasks(ctx context.Context, filter *repo.TaskFilter) ([]*repo.Task, error) {
	tasks, err := s.repo.GetAll(ctx, filter)
	if err != nil {
		return nil, fmt.Errorf("ошибка при получении списка задач: %w", err)
	}
	return tasks, nil
}

func (s *taskService) GetTaskByID(ctx context.Context, id int) (*repo.Task, error) {
	task, err := s.repo.GetByID(ctx, id)
	if err != nil {
		return nil, fmt.Errorf("ошибка при получении задачи: %w", err)
	}

	return task, nil
}
