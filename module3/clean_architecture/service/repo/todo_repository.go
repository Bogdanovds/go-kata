package repo

import (
	"context"
	"fmt"
)

type Task struct {
	ID          int
	Title       string
	Description string
	Status      string
}

type TaskFilter struct {
	Status string
}

type TaskRepository interface {
	Create(ctx context.Context, task *Task) error
	Update(ctx context.Context, task *Task) error
	Delete(ctx context.Context, id int) error
	GetAll(ctx context.Context, filter *TaskFilter) ([]*Task, error)
	GetByID(ctx context.Context, id int) (*Task, error)
}

type taskRepository struct {
	tasks []*Task
}

func NewTaskRepository() TaskRepository {
	return &taskRepository{
		tasks: make([]*Task, 0),
	}
}

func (r *taskRepository) Create(ctx context.Context, task *Task) error {
	r.tasks = append(r.tasks, task)
	return nil
}

func (r *taskRepository) Update(ctx context.Context, task *Task) error {
	for i, t := range r.tasks {
		if t.ID == task.ID {
			r.tasks[i] = task
			return nil
		}
	}
	return fmt.Errorf("task with ID %d not found", task.ID)
}

func (r *taskRepository) Delete(ctx context.Context, id int) error {
	for i, t := range r.tasks {
		if t.ID == id {
			r.tasks = append(r.tasks[:i], r.tasks[i+1:]...)
			return nil
		}
	}
	return fmt.Errorf("task with ID %d not found", id)
}

func (r *taskRepository) GetAll(ctx context.Context, filter *TaskFilter) ([]*Task, error) {
	if filter == nil {
		return r.tasks, nil
	}
	result := make([]*Task, 0)
	for _, t := range r.tasks {
		if filter.Status == "" || t.Status == filter.Status {
			result = append(result, t)
		}
	}
	return result, nil
}

func (r *taskRepository) GetByID(ctx context.Context, id int) (*Task, error) {
	for _, t := range r.tasks {
		if t.ID == id {
			return t, nil
		}
	}
	return nil, fmt.Errorf("task with ID %d not found", id)
}
