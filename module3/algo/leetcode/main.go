package leetcode

import (
	"fmt"
	"math"
	"sort"
	"strings"
)

// 1. N-th Tribonacci Number
func tribonacci(n int) int {
	if n == 0 {
		return 0
	}
	if n == 1 || n == 2 {
		return 1
	}
	t0, t1, t2 := 0, 1, 1
	tn := 0
	for i := 3; i <= n; i++ {
		tn = t0 + t1 + t2
		t0 = t1
		t1 = t2
		t2 = tn
	}
	return tn
}

// 2. Объединение массива
func getConcatenation(nums []int) []int {
	n := len(nums)
	ans := make([]int, 2*n)
	for i := 0; i < n; i++ {
		ans[i] = nums[i]
		ans[i+n] = nums[i]
	}
	return ans
}

// 3. Преобразование температуры
func convertTemperature(celsius float64) []float64 {
	kelvin := celsius + 273.15
	fahrenheit := celsius*1.8 + 32
	return []float64{kelvin, fahrenheit}
}

// 4. Построить массив из перестановки
func buildArray(nums []int) []int {
	n := len(nums)
	ans := make([]int, n)
	for i := 0; i < n; i++ {
		ans[i] = nums[nums[i]]
	}
	return ans
}

// 5. Количество матчей в турнире
func numberOfMatches(n int) int {
	if n == 1 {
		return 0
	}
	if n%2 == 0 {
		return n/2 + numberOfMatches(n/2)
	} else {
		return (n-1)/2 + numberOfMatches((n-1)/2+1)
	}
}

// 6. Уникальные слова Морзе
func uniqueMorseRepresentations(words []string) int {
	morseCode := []string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}
	morseMap := make(map[string]bool)
	for _, word := range words {
		var morseWord string
		for _, ch := range word {
			morseWord += morseCode[ch-'a']
		}
		morseMap[morseWord] = true
	}
	return len(morseMap)
}

// 7. Защита IP-адреса
func defangIPaddr(address string) string {
	result := strings.ReplaceAll(address, ".", "[.]")
	return result
}

// 8. K-й пропущенный положительный номер
func findKthPositive(arr []int, k int) int {
	left, right := 0, len(arr)
	for left < right {
		middle := (left + right) / 2
		if arr[middle]-middle-1 < k {
			left = middle + 1
		} else {
			right = middle
		}
	}
	return k + left
}

// 9. Финальное значение переменной после выполнения операций
func finalValueAfterOperations(operations []string) int {
	x := 0
	for _, op := range operations {
		if op == "++X" || op == "X++" {
			x++
		} else {
			x--
		}
	}
	return x
}

// 10. Перетасовать массив
func shuffle(nums []int, n int) []int {
	res := make([]int, 2*n)
	for i := 0; i < n; i++ {
		res[2*i] = nums[i]
		res[2*i+1] = nums[i+n]
	}
	return res
}

// 11. Текущее значение суммы в одномерном массиве
func runningSum(nums []int) []int {
	sum := 0
	result := make([]int, len(nums))
	for i, num := range nums {
		sum += num
		result[i] = sum
	}
	return result
}

// 12. Количество хороших пар
func numIdenticalPairs(nums []int) int {
	count := 0
	freq := make(map[int]int)
	for _, num := range nums {
		count += freq[num]
		freq[num]++
	}
	return count
}

// 13. Драгоценности и камни
func numJewelsInStones(jewels string, stones string) int {
	count := 0

	for _, stone := range stones {
		for _, jewel := range jewels {
			if stone == jewel {
				count++
				break
			}
		}
	}

	return count
}

// 14. Богатство самого богатого клиента
func maximumWealth(accounts [][]int) int {
	maxWealth := 0
	for _, customer := range accounts {
		customerWealth := 0
		for _, account := range customer {
			customerWealth += account
		}
		if customerWealth > maxWealth {
			maxWealth = customerWealth
		}
	}
	return maxWealth
}

// 15. Дизайн парковочной системы
type ParkingSystem struct {
	big, medium, small int
}

func Constructor(big int, medium int, small int) ParkingSystem {
	return ParkingSystem{big, medium, small}
}

func (this *ParkingSystem) AddCar(carType int) bool {
	switch carType {
	case 1:
		if this.big > 0 {
			this.big--
			return true
		}
	case 2:
		if this.medium > 0 {
			this.medium--
			return true
		}
	case 3:
		if this.small > 0 {
			this.small--
			return true
		}
	}
	return false
}

// 16. Наименьшее четное кратное
func smallestEvenMultiple(n int) int {
	multiple := n
	for multiple%2 != 0 || multiple%n != 0 {
		multiple += n
	}
	return multiple
}

// 17. Максимальное количество найденных слов в предложениях
func mostWordsFound(sentences []string) int {
	maxWords := 0
	for _, sentence := range sentences {
		words := strings.Split(sentence, " ")
		numWords := len(words)
		if numWords > maxWords {
			maxWords = numWords
		}
	}
	return maxWords
}

// 18. Разница между суммой элементов и суммой цифр массива
func differenceOfSum(nums []int) int {
	sumOfNums := 0
	sumOfDigits := 0
	for _, num := range nums {
		sumOfNums += num
		for num > 0 {
			sumOfDigits += num % 10
			num /= 10
		}
	}
	n := sumOfNums - sumOfDigits
	if n < 0 {
		return -n
	}
	return n
}

// 19. Минимальная сумма четырехзначного числа после разделения цифр
func minimumSum(num int) int {
	digits := make([]int, 4)
	for i := 0; i < 4; i++ {
		digits[i] = num % 10
		num /= 10
	}
	for i, j := 0, 3; i < j; i, j = i+1, j-1 {
		digits[i], digits[j] = digits[j], digits[i]
	}
	permutations := make([][]int, 0)
	visited := make(map[string]bool)
	backtrack(digits, &permutations, visited, []int{})
	minSum := math.MaxInt64
	for _, perm := range permutations {
		new1 := perm[0]*10 + perm[1]
		new2 := perm[2]*10 + perm[3]
		sum := new1 + new2
		if sum < minSum {
			minSum = sum
		}
	}
	return minSum
}

func backtrack(digits []int, permutations *[][]int, visited map[string]bool, current []int) {
	if len(current) == 4 {
		key := fmt.Sprint(current)
		if !visited[key] {
			*permutations = append(*permutations, append([]int{}, current...))
			visited[key] = true
		}
		return
	}
	for i := 0; i < len(digits); i++ {
		if visited[fmt.Sprint(i)] {
			continue
		}
		visited[fmt.Sprint(i)] = true
		current = append(current, digits[i])
		backtrack(digits, permutations, visited, current)
		current = current[:len(current)-1]
		visited[fmt.Sprint(i)] = false
	}
}

// 20. Дети с наибольшим количеством конфет
func kidsWithCandies(candies []int, extraCandies int) []bool {
	maxCandies := 0
	for _, c := range candies {
		if c > maxCandies {
			maxCandies = c
		}
	}
	result := make([]bool, len(candies))
	for i, c := range candies {
		if c+extraCandies >= maxCandies {
			result[i] = true
		}
	}
	return result
}

// 21. Разница между произведением и суммой цифр целого числа
func subtractProductAndSum(n int) int {
	digits := make([]int, 0)
	sum, prod := 0, 1
	for n > 0 {
		digit := n % 10
		digits = append(digits, digit)
		sum += digit
		prod *= digit
		n /= 10
	}
	return prod - sum
}

// 22. Сколько чисел меньше, чем текущее число
func smallerNumbersThanCurrent(nums []int) []int {
	res := make([]int, len(nums))

	count := make(map[int]int)
	for _, num := range nums {
		count[num]++
	}

	for i := 1; i <= 100; i++ {
		count[i] += count[i-1]
	}

	for i, num := range nums {
		if num == 0 {
			res[i] = 0
		} else {
			res[i] = count[num-1]
		}
	}

	return res
}

// 23. Интерпретация синтаксического анализатора цели
func interpret(command string) string {
	var res strings.Builder
	for i := 0; i < len(command); i++ {
		if command[i] == 'G' {
			res.WriteByte('G')
		} else if command[i] == '(' && command[i+1] == ')' {
			res.WriteString("o")
			i++
		} else if command[i] == '(' && command[i+1] == 'a' {
			res.WriteString("al")
			i += 3
		}
	}
	return res.String()
}

// 24. Декодирование XOR-массива
func decode(encoded []int, first int) []int {
	n := len(encoded) + 1
	arr := make([]int, n)
	arr[0] = first
	for i := 1; i < n; i++ {
		arr[i] = arr[i-1] ^ encoded[i-1]
	}
	return arr
}

// MEDIUM

// 1. Сумма самых глубоких листьев.
func deepestLeavesSum(root *TreeNode) int {
	if root == nil {
		return 0
	}
	queue := []*TreeNode{root}
	var sum int
	for len(queue) != 0 {
		size := len(queue)
		sum = 0
		for i := 0; i < size; i++ {
			node := queue[0]
			queue = queue[1:]
			sum += node.Val
			if node.Left != nil {
				queue = append(queue, node.Left)
			}
			if node.Right != nil {
				queue = append(queue, node.Right)
			}
		}
	}
	return sum
}

// 2. Отсортируйте студентов по их K-му баллу.
func sortTheStudents(score [][]int, k int) [][]int {
	indexes := make([]int, len(score))
	for i := range indexes {
		indexes[i] = i
	}

	sort.Slice(indexes, func(i, j int) bool {
		return score[indexes[i]][k] > score[indexes[j]][k]
	})

	sortedScore := make([][]int, len(score))
	for i, idx := range indexes {
		sortedScore[i] = score[idx]
	}

	return sortedScore
}

// 3. Объединить узлы

func mergeNodes(head *ListNode) *ListNode {
	if head == nil || head.Next == nil {
		return head
	}

	dummy := &ListNode{Val: 0, Next: head}
	prevZero := dummy
	curr := head
	sum := 0

	for curr != nil {
		if curr.Val == 0 {
			if sum > 0 {
				prevZero.Next = &ListNode{Val: sum, Next: nil}
				prevZero = prevZero.Next
			}
			sum = 0
		} else {
			sum += curr.Val
		}
		curr = curr.Next
	}

	if sum > 0 {
		prevZero.Next = &ListNode{Val: sum, Next: nil}
	} else {
		prevZero.Next = nil
	}

	return dummy.Next
}

// 4. Бинарное дерево в большую сумму дерева.
func bstToGst(root *TreeNode) *TreeNode {
	sum := 0
	var traverse func(*TreeNode)
	traverse = func(node *TreeNode) {
		if node == nil {
			return
		}
		traverse(node.Right)

		sum += node.Val
		node.Val = sum

		traverse(node.Left)
	}
	traverse(root)
	return root
}

// 5. Максимальная сумма двух звеньев связанного списка
func pairSum(head *ListNode) int {
	maxSum := 0
	slow, fast := head, head
	for fast != nil && fast.Next != nil {
		slow = slow.Next
		fast = fast.Next.Next
	}
	var reversed *ListNode
	for slow != nil {
		slow.Next, reversed, slow = reversed, slow, slow.Next
	}
	for head != nil && reversed != nil {
		sum := head.Val + reversed.Val
		if sum > maxSum {
			maxSum = sum
		}
		head, reversed = head.Next, reversed.Next
	}
	return maxSum
}

// 6. Максимальное бинарное дерево
func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}

	maxIdx := 0
	for i := range nums {
		if nums[i] > nums[maxIdx] {
			maxIdx = i
		}
	}

	root := &TreeNode{Val: nums[maxIdx]}

	root.Left = constructMaximumBinaryTree(nums[:maxIdx])
	root.Right = constructMaximumBinaryTree(nums[maxIdx+1:])

	return root
}

// 7. Балансировка бинарного поискового дерева
func balanceBST(root *TreeNode) *TreeNode {
	var values []int
	var inorderTraversal func(node *TreeNode)
	inorderTraversal = func(node *TreeNode) {
		if node == nil {
			return
		}
		inorderTraversal(node.Left)
		values = append(values, node.Val)
		inorderTraversal(node.Right)
	}

	var buildBalancedBST func(left, right int) *TreeNode
	buildBalancedBST = func(left, right int) *TreeNode {
		if left > right {
			return nil
		}
		mid := (left + right) / 2
		node := &TreeNode{Val: values[mid]}
		node.Left = buildBalancedBST(left, mid-1)
		node.Right = buildBalancedBST(mid+1, right)
		return node
	}

	inorderTraversal(root)
	return buildBalancedBST(0, len(values)-1)
}

// 8. Минимальное количество вершин для достижения всех узлов
func findSmallestSetOfVertices(n int, edges [][]int) []int {
	incoming := make([]int, n)
	for _, edge := range edges {
		incoming[edge[1]]++
	}
	result := make([]int, 0)
	for i := 0; i < n; i++ {
		if incoming[i] == 0 {
			result = append(result, i)
		}
	}
	return result
}

// 9. Арифметические подмассивы
func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	var res []bool
	q := len(l)
	for i := 0; i < q; i++ {
		arr := nums[l[i] : r[i]+1]
		var w []int
		w = append(w, arr...)
		sort.Ints(w)
		n := len(w)
		if n <= 2 {
			res = append(res, true)
			continue
		}
		f := w[n-1] - w[n-2]
		for i := n - 1; i > 0; i-- {
			if w[i]-w[i-1] != f {
				res = append(res, false)
				break
			}
			if i == 1 {
				res = append(res, true)
			}
		}
	}
	return res
}

// 10. Возможности буквенных плиток.
func numTilePossibilities(tiles string) int {
	count := make(map[string]int)
	for _, c := range tiles {
		count[string(c)]++
	}
	return backtrack(count)
}

func backtrack(count map[string]int) int {
	sum := 0
	for c, n := range count {
		if n > 0 {
			count[c]--
			sum += 1 + backtrack(count)
			count[c]++
		}
	}
	return sum
}

// 11. Удалить листья с заданным значением
func removeLeafNodes(root *TreeNode, target int) *TreeNode {
	if root == nil {
		return nil
	}

	root.Left = removeLeafNodes(root.Left, target)
	root.Right = removeLeafNodes(root.Right, target)

	if root.Left == nil && root.Right == nil && root.Val == target {
		return nil
	}

	if root.Left == nil && root.Right == nil {
		return root
	}

	return root
}

// 12. Объединить между связанными списками
func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	var prevA, nodeB *ListNode
	curr := list1
	index := 0
	for curr != nil {
		if index == a-1 {
			prevA = curr
		}
		if index == b {
			nodeB = curr.Next
			break
		}
		curr = curr.Next
		index++
	}
	prevA.Next = list2
	for list2.Next != nil {
		list2 = list2.Next
	}
	list2.Next = nodeB
	return list1
}

// 13. Найти максимальную сумму песочных часов
func maxSum(grid [][]int) int {
	m := len(grid)
	n := len(grid[0])
	maxSum := math.MinInt32

	for i := 0; i <= m-3; i++ {
		for j := 0; j <= n-3; j++ {
			sum := grid[i][j] + grid[i][j+1] + grid[i][j+2] +
				grid[i+1][j+1] +
				grid[i+2][j] + grid[i+2][j+1] + grid[i+2][j+2]
			if sum > maxSum {
				maxSum = sum
			}
		}
	}

	return maxSum
}

// 14. Запросы XOR подмассива
func xorQueries(arr []int, queries [][]int) []int {
	n := len(arr)
	prefixXor := make([]int, n+1)
	for i := 0; i < n; i++ {
		prefixXor[i+1] = prefixXor[i] ^ arr[i]
	}
	m := len(queries)
	result := make([]int, m)
	for i := 0; i < m; i++ {
		left := queries[i][0]
		right := queries[i][1]
		result[i] = prefixXor[right+1] ^ prefixXor[left]
	}
	return result
}

// 15. Разбиение на минимальное количество децимальных двоичных чисел
func minPartitions(n string) int {
	maxDigit := 0
	for _, ch := range n {
		digit := int(ch - '0')
		if digit > maxDigit {
			maxDigit = digit
		}
	}
	return maxDigit
}

// 16. Запросы подпрямоугольника
type SubrectangleQueries struct {
	rectangle [][]int
}

func Constructor(rectangle [][]int) SubrectangleQueries {
	return SubrectangleQueries{rectangle}
}

func (this *SubrectangleQueries) UpdateSubrectangle(row1 int, col1 int, row2 int, col2 int, newValue int) {
	for i := row1; i <= row2; i++ {
		for j := col1; j <= col2; j++ {
			this.rectangle[i][j] = newValue
		}
	}
}

func (this *SubrectangleQueries) GetValue(row int, col int) int {
	return this.rectangle[row][col]
}
