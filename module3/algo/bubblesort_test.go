package algo

import (
	"reflect"
	"testing"
)

func TestBubblesort(t *testing.T) {
	type args struct {
		data []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "sort reserved slice",
			args: args{
				data: []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1},
			},
			want: []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Bubblesort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Bubblesort() = #{got}, want #{tt.want}")
			}
		})
	}
}
func BenchmarkBubblesort(b *testing.B) {
	data := generateData(b.N, 1000, 5000)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		Bubblesort(data[i])
	}
}
