package algo

import "math/rand"

func Quicksort(arr []int) []int {

	if len(arr) < 1 {
		return arr
	}

	median := arr[rand.Intn(len(arr))] // Находим медиану, выбирая случайное число

	low_part := make([]int, 0, len(arr))    // Сюда записываем числа меньше числа в медиане (pivot)
	high_part := make([]int, 0, len(arr))   // Сюда записываем числа больше числа в медиане (pivot)
	middle_part := make([]int, 0, len(arr)) // Здесь будут храниться числа равные медиане (pivot)

	for _, item := range arr {
		switch {
		case item < median:
			low_part = append(low_part, item) // Запись чисел меньше медианы
		case item == median:
			middle_part = append(middle_part, item) // Запись чисел равных медиане
		case item > median:
			high_part = append(high_part, item) // Запись чисел больше медианы
		}
	}

	low_part = Quicksort(low_part)
	high_part = Quicksort(high_part)

	low_part = append(low_part, middle_part...) // Распаковываем среднюю часть после всех чисел
	low_part = append(low_part, high_part...)   // Далее распаковываем оставшуюся часть

	return low_part

}
