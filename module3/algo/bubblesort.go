package algo

func Bubblesort(data []int) []int {
	swapped := true // Создаем переменную для проверки обмена местами в цикле
	i := 1          // Выносим переменную отвечающую за изменение длины сортируемой части массива
	for swapped {
		swapped = false                    // По умолчанию говорим, что обмена не было в цикле
		for j := 0; j < len(data)-i; j++ { // len(data)-i не тратим время на отсортированная часть
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swapped = true // Ставим swapped в true, если значения поменялись местами
			}
		}
		i++ // Инкрементируем индекс для пропуска отсортированной части
	}

	return data
}
