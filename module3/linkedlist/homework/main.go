package main

import (
	"errors"
	"fmt"
	"math/rand"

	"gopkg.in/loremipsum.v1"
)

type Post struct {
	body        string
	publishDate int64
	next        *Post
}

type Feed struct {
	length int
	start  *Post
	end    *Post
}

func (feed *Feed) Append(newPost *Post) {
	if feed.length == 0 {
		feed.start = newPost
		feed.end = newPost
	} else {
		newPost.publishDate = feed.end.publishDate + 1
		feed.end.next = newPost
		feed.end = newPost
	}
	feed.length++
}

func (feed *Feed) Remove(publishDate int64) {
	if feed.length <= 0 {
		panic(errors.New("feed is empty"))
	}

	var prevPost *Post
	post := feed.start

	for post.publishDate != publishDate {
		if post.next == nil {
			panic(errors.New("missing Post with specified publishDate"))
		}

		prevPost = post
		post = prevPost.next
	}

	if feed.end == post {
		feed.end = prevPost
	}
	prevPost.next = post.next

	feed.length--
}

func (feed *Feed) Insert(newPost *Post) {
	if feed.length == 0 {
		feed.start = newPost
		feed.end = newPost
		return
	} else if feed.end.publishDate < newPost.publishDate {
		feed.end.next = newPost
		feed.end = newPost
	} else if feed.start.publishDate >= newPost.publishDate {
		newPost.next = feed.start
		feed.start = newPost
	} else {
		var prevPost *Post
		post := feed.start

		for post.publishDate < newPost.publishDate {
			prevPost = post
			post = prevPost.next
		}

		newPost.next = post
		prevPost.next = newPost
	}
	feed.length++
}

func (feed *Feed) Inspect() {
	fmt.Printf("number of posts: %d\n", feed.length)
	post := feed.start
	for i := 0; i < feed.length; i++ {
		fmt.Printf("Post %d\n", i)
		fmt.Printf("	Body %s\n", post.body)
		fmt.Printf("	Date: %d\n", post.publishDate)
		fmt.Printf("	Next post: %v\n", post.next)
		post = post.next
	}
}

func main() {
	loremIpsumGeneratoe := loremipsum.New()
	var feed Feed

	for i := 0; i < rand.Intn(25); i++ {
		feed.Append(&Post{body: loremIpsumGeneratoe.Words(rand.Intn(10)), publishDate: rand.Int63()})
	}

	feed.Inspect()

	for i := 0; i < rand.Intn(25); i++ {
		feed.Insert(&Post{body: loremIpsumGeneratoe.Words(rand.Intn(10)), publishDate: rand.Int63()})
	}

	feed.Inspect()
}
