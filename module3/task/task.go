package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"time"

	"github.com/brianvoe/gofakeit"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index()
	Pop()
	Shift() *Node
	SearchUUID(uuID string) *Node
	Search(message string) *Node
	Reverse() *DoubleLinkedList
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func (d *DoubleLinkedList) LoadData(path string) error {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	var commits []Commit
	if err := json.Unmarshal([]byte(data), &commits); err != nil {
		return err
	}

	commits = QuickSort(commits)

	nodes := make([]*Node, len(commits))

	for i, commit := range commits {
		node := &Node{data: &Commit{
			Message: commit.Message,
			UUID:    commit.UUID,
			Date:    commit.Date,
		}}
		nodes[i] = node
	}

	for i, node := range nodes {
		if i == 0 {
			d.head = node
			d.tail = node
		} else {
			node.prev = d.tail
			d.tail.next = node
			d.tail = node
		}
		d.len++
	}

	d.curr = d.head
	return nil
}

func (d *DoubleLinkedList) Len() int {
	return d.len
}

func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil {
		return nil
	}

	prev := d.curr.prev
	d.curr = prev
	return prev
}

func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil {
		return nil
	}

	next := d.curr.next
	d.curr = next
	return next

}

func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if n < 0 || n >= d.len {
		return fmt.Errorf("index out of range")
	}

	newNode := &Node{data: &c}

	if n == d.len-1 {
		newNode.prev = d.tail
		d.tail.next = newNode
		d.tail = newNode
	} else {
		node := d.head
		for i := 0; i < n-2; i++ {
			node = node.next
		}

		newNode.prev = node
		newNode.next = node.next
		node.next.prev = newNode
		node.next = newNode
	}

	d.len++
	return nil
}

func (d *DoubleLinkedList) Delete(n int) error {
	if n < 0 || n >= d.len {
		return fmt.Errorf("index out of range")
	}

	if n == 0 {
		d.head = d.head.next
		if d.head != nil {
			d.head.prev = nil
		} else {
			d.tail = nil
		}
	} else if n == d.len-1 {
		d.tail = d.tail.prev
		d.tail.next = nil
	} else {

		node := d.head
		for i := 0; i < n; i++ {
			node = node.next
		}
		node.prev.next = node.next
		node.next.prev = node.prev
	}

	d.len--
	return nil
}

func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return fmt.Errorf("current node is nil")
	}

	if d.curr == d.head {
		d.head = d.head.next
		if d.head != nil {
			d.head.prev = nil
		} else {
			d.tail = nil
		}
	} else if d.curr == d.tail {
		d.tail = d.tail.prev
		d.tail.next = nil
	} else {
		d.curr.prev.next = d.curr.next
		d.curr.next.prev = d.curr.prev
	}

	d.curr = d.curr.next
	d.len--
	return nil
}

func (d *DoubleLinkedList) Index() (int, error) {
	if d.curr == nil {
		return -1, fmt.Errorf("current node is nil")
	}

	index := 0
	node := d.head
	for node != d.curr {
		index++
		node = node.next
	}

	return index, nil
}

func (d *DoubleLinkedList) Pop() *Node {
	if d.tail == nil {
		return nil
	}

	node := d.tail

	if d.tail.prev != nil {
		d.tail = d.tail.prev
		d.tail.next = nil
	} else {
		d.head = nil
		d.tail = nil
	}

	if d.curr == node {
		d.curr = d.tail
	}
	d.len--
	return node
}

func (d *DoubleLinkedList) Shift() *Node {
	if d.head == nil {
		return nil
	}

	node := d.head
	d.head = d.head.next

	if d.head != nil {
		d.head.prev = nil
	} else {
		d.tail = nil
	}

	if d.curr == node {
		d.curr = d.head
	}

	return node
}

func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	node := d.head
	for node != nil {
		if node.data.UUID == uuID {
			return node
		}
		node = node.next
	}
	return nil
}

func (d *DoubleLinkedList) Search(message string) *Node {
	node := d.head
	for node != nil {
		if node.data.Message == message {
			return node
		}
		node = node.next
	}
	return nil
}

func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	node := d.head
	var prev *Node

	for node != nil {
		next := node.next
		node.next = prev
		node.prev = next
		prev = node
		node = next
	}
	d.head, d.tail = d.tail, d.head
	return d
}

func QuickSort(commits []Commit) []Commit {

	if len(commits) < 2 {
		return commits
	}

	rand.Seed(time.Now().UnixNano())
	median := rand.Intn(len(commits))

	left, right := 0, len(commits)-1

	commits[median], commits[right] = commits[right], commits[median]

	for i := range commits {
		if commits[i].Date.Before(commits[right].Date) {
			commits[i], commits[left] = commits[left], commits[i]
			left++
		}
	}

	commits[left], commits[right] = commits[right], commits[left]

	QuickSort(commits[:left])
	QuickSort(commits[left+1:])
	return commits

}

func GenerateJSON(numCommits int) []Commit {
	gofakeit.Seed(time.Now().UnixNano())
	var commits []Commit

	for i := 0; i < numCommits; i++ {
		commit := Commit{
			Message: gofakeit.Sentence(10),
			UUID:    gofakeit.UUID(),
			Date:    gofakeit.DateRange(time.Now().Add(-time.Hour*24*365), time.Now()),
		}

		commits = append(commits, commit)
	}

	return commits
}

func GenData(n int) (*DoubleLinkedList, error) {
	d := &DoubleLinkedList{}
	commits := GenerateJSON(n)
	commits = QuickSort(commits)
	nodes := make([]*Node, len(commits))

	for i, commit := range commits {
		node := &Node{data: &Commit{
			Message: commit.Message,
			UUID:    commit.UUID,
			Date:    commit.Date,
		}}
		nodes[i] = node
	}

	for i, node := range nodes {
		if i == 0 {
			d.head = node
			d.tail = node
		} else {
			node.prev = d.tail
			d.tail.next = node
			d.tail = node
		}
		d.len++
	}

	d.curr = d.head
	return d, nil
}
