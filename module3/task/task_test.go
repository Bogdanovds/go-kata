package main

import (
	"encoding/json"
	"io/ioutil"
	"math/rand"
	"os"
	"testing"
	"time"

	"github.com/brianvoe/gofakeit"
)

func Test_LoadData(t *testing.T) {
	commits := GenerateJSON(10)

	data, err := json.Marshal(commits)
	if err != nil {
		t.Fatalf("Failed to marshal test data: %v", err)
	}

	tmpfile, err := ioutil.TempFile("", "test-commit-data-*.json")
	if err != nil {
		t.Fatalf("Failed to create temp file: %v", err)
	}

	defer os.Remove(tmpfile.Name())
	if _, err := tmpfile.Write(data); err != nil {
		t.Fatalf("Failed to write test data to temp file: %v", err)
	}

	if err := tmpfile.Close(); err != nil {
		t.Fatalf("Failed to close temp file: %v", err)
	}

	d := &DoubleLinkedList{}
	if err := d.LoadData(tmpfile.Name()); err != nil {
		t.Fatalf("Failed to load data: %v", err)
	}

	if d.Len() != len(commits) {
		t.Errorf("Expected list length %d, got %d", len(commits), d.Len())
	}

	sortedCommits := QuickSort(commits)

	node := d.head
	for i := 0; i < d.Len(); i++ {
		if node.data.Message != sortedCommits[i].Message ||
			node.data.UUID != sortedCommits[i].UUID ||
			!node.data.Date.Equal(sortedCommits[i].Date) {
			t.Errorf("Expected commit at position %d to be %v, got %v", i, sortedCommits[i], *node.data)
		}
		node = node.next
	}
}

func Test_Len(t *testing.T) {
	d, err := GenData(10000)
	if err != nil {
		t.Fatalf("failed to genData: %v", err)
	}
	expectedLen := 10000
	actualLen := d.Len()

	if actualLen != expectedLen {
		t.Errorf("Len() returned %d, expected %d", actualLen, expectedLen)
	}
}

func Test_Current(t *testing.T) {
	d, err := GenData(10000)
	if err != nil {
		t.Fatalf("failed to genData: %v", err)
	}

	if d.Current() != d.head {
		t.Errorf("Expected current element to be head node, got %v", d.Current())
	}

	d.Next()

	if d.Current() != d.head.next {
		t.Errorf("Expected current element to be second node, got %v", d.Current())
	}

	d.Prev()

	if d.Current() != d.head {
		t.Errorf("Expected current element to be head node, got %v", d.Current())
	}
}

func Test_Prev(t *testing.T) {
	d := &DoubleLinkedList{}
	if d.Prev() != nil {
		t.Errorf("Prev() on empty list should return nil")
	}

	d, err := GenData(10000)
	if err != nil {
		t.Fatalf("failed to genData: %v", err)
	}
	d.curr = d.head
	if d.Prev() != nil {
		t.Errorf("Prev() on first element should return nil")
	}
	d.curr = d.tail
	prev := d.Prev()
	if prev != d.tail.prev {
		t.Errorf("Prev() returned wrong element, expected %v but got %v", d.tail.prev, prev)
	}
}

func Test_Next(t *testing.T) {
	d := &DoubleLinkedList{}
	if d.Prev() != nil {
		t.Errorf("Next() on empty list should return nil")
	}

	d, err := GenData(10000)
	if err != nil {
		t.Fatalf("failed to genData: %v", err)
	}

	d.Reverse()
	if d.Next() != nil {
		t.Errorf("Next() on last element should return nil")
	}
	d.Reverse()

	d.curr = d.head
	next := d.Next()
	if next != d.head.next {
		t.Errorf("Next() returned wrong element, expected %v but got %v", d.head.next, next)
	}
}

func Test_Insert(t *testing.T) {
	d, err := GenData(10000)
	if err != nil {
		t.Fatalf("Failed to create data: %v", err)
	}

	newCommit := Commit{
		Message: "Test Commit",
		UUID:    gofakeit.UUID(),
		Date:    time.Now(),
	}
	r := rand.Intn(d.Len())
	err = d.Insert(r, newCommit)
	if err != nil {
		t.Fatalf("Failed to insert commit: %v", err)
	}
	node := d.Current()

	for i := 0; i < d.Len(); i++ {
		if node.data.Message == "Test Commit" {
			if i != r-1 {
				t.Fatalf("Failed to insert commit at correct position")
			}
			return
		}
		node = d.Next()
	}

	t.Fatalf("Failed to find inserted commit")
}

func Test_Delete(t *testing.T) {
	d, err := GenData(10)
	if err != nil {
		t.Fatalf("Failed to create data: %v", err)
	}
	len := d.Len()
	message := d.head.next.data.Message
	if err := d.Delete(0); err != nil {
		t.Errorf("Error deleting node: %v", err)
	}

	if d.len != len-1 {
		t.Errorf("Expected length to be %d, but got %d", len-1, d.len)
	}

	if d.head.data.Message != message {
		t.Errorf("Expected head's message to be '%s', but got '%s'", message, d.head.data.Message)
	}
}

func Test_DeleteCurrent(t *testing.T) {
	d, err := GenData(10)
	if err != nil {
		t.Fatalf("Failed to create data: %v", err)
	}
	curr := d.Current()
	index, err := d.Index()
	if err != nil {
		t.Fatalf("Failed to get index: %v", err)
	}

	err = d.DeleteCurrent()
	if err != nil {
		t.Fatalf("Failed to delete current node: %v", err)
	}

	newCurr := d.Current()
	if newCurr != curr.next {
		t.Fatalf("Expected current node to be updated, but got %v", newCurr)
	}

	expectedLen := 9
	if d.Len() != expectedLen {
		t.Fatalf("Expected length of list to be %v, but got %v", expectedLen, d.Len())
	}

	newIndex, err := d.Index()
	if err != nil {
		t.Fatalf("Failed to get index: %v", err)
	}
	expectedIndex := index
	if index == d.Len() {
		expectedIndex--
	}
	if newIndex != expectedIndex {
		t.Fatalf("Expected index to be %v, but got %v", expectedIndex, newIndex)
	}
}

func Test_Index(t *testing.T) {
	d, err := GenData(10000)
	if err != nil {
		t.Fatalf("Failed to create data: %v", err)
	}

	index, err := d.Index()
	if err != nil {
		t.Fatalf("Error calling Index() method: %v", err)
	}

	if index != 0 {
		t.Errorf("Expected index 0 for initial element, but got index %d", index)
	}

	d.Next()
	index, err = d.Index()
	if err != nil {
		t.Fatalf("Error calling Index() method: %v", err)
	}

	if index != 1 {
		t.Errorf("Expected index 1 for next element, but got index %d", index)
	}

	d.Prev()
	index, err = d.Index()
	if err != nil {
		t.Fatalf("Error calling Index() method: %v", err)
	}

	if index != 0 {
		t.Errorf("Expected index 0 for initial element after calling Prev(), but got index %d", index)
	}
}

func Test_Pop(t *testing.T) {
	d, err := GenData(10)
	if err != nil {
		t.Fatalf("Failed to create data: %v", err)
	}

	originalLen := d.Len()

	lastNode := d.Pop()
	if lastNode == nil {
		t.Fatalf("Failed to pop last node")
	}
	if lastNode.data.UUID == d.tail.data.UUID {
		t.Fatalf("Last node UUID does not match tail UUID")
	}

	if d.Len() != originalLen-1 {
		t.Fatalf("List length did not decrease after pop")
	}

	for i := 0; i < originalLen-1; i++ {
		if d.Pop() == nil {
			t.Fatalf("Failed to pop node")
		}
	}
	if d.Len() != 0 {
		t.Fatalf("List is not empty after popping all nodes")
	}
}

func Test_Shift(t *testing.T) {
	d, err := GenData(10)
	if err != nil {
		t.Fatalf("Failed to create data: %v", err)
	}
	expectedMessage := d.curr.data.Message
	expectedUUID := d.curr.data.UUID
	expectedDate := d.curr.data.Date
	expectedLen := d.Len()
	node := d.Shift()

	if node.data.Message != expectedMessage {
		t.Errorf("Expected message %q, got %q", expectedMessage, node.data.Message)
	}

	if node.data.UUID != expectedUUID {
		t.Errorf("Expected UUID %q, got %q", expectedUUID, node.data.UUID)
	}

	if !node.data.Date.Equal(expectedDate) {
		t.Errorf("Expected date %v, got %v", expectedDate, node.data.Date)
	}

	if d.Len() != expectedLen {
		t.Errorf("Expected length %d, got %d", expectedLen, d.Len())
	}

	if d.Current() != d.head {
		t.Errorf("Expected current element to be the new head of the list")
	}
}

func Test_SearchUUID(t *testing.T) {
	d, err := GenData(10)
	if err != nil {
		t.Fatalf("Failed to create data: %v", err)
	}
	node := d.Current()
	commits := GenerateJSON(1)
	nullUUID := commits[0].UUID
	for i := 0; i < rand.Intn(d.Len()); i++ {
		node = d.Next()
	}
	someUUID := node.data.UUID

	resultNode := d.SearchUUID(someUUID)
	if resultNode == nil {
		t.Errorf("SearchUUID should have found a node")
	}
	if resultNode.data.UUID != someUUID {
		t.Errorf("SearchUUID should have found the correct node")
	}

	resultNode = d.SearchUUID(nullUUID)
	if resultNode != nil {
		t.Errorf("SearchUUID should not have found a node")
	}
}

func Test_Search(t *testing.T) {
	d, err := GenData(10)
	if err != nil {
		t.Fatalf("Failed to create data: %v", err)
	}
	node := d.Current()
	commits := GenerateJSON(1)
	nullMessage := commits[0].Message
	for i := 0; i < rand.Intn(d.Len()); i++ {
		node = d.Next()
	}

	someMessage := node.data.Message

	resultNode := d.Search(someMessage)
	if resultNode == nil {
		t.Errorf("Search should have found a node")
	}

	if resultNode.data.Message != someMessage {
		t.Errorf("Search should have found the correct node")
	}

	resultNode = d.SearchUUID(nullMessage)
	if resultNode != nil {
		t.Errorf("Search should not have found a node")
	}
}

func Test_Reverse(t *testing.T) {
	d, err := GenData(10)
	if err != nil {
		t.Fatalf("Failed to create data: %v", err)
	}

	firstNode := d.head.data
	lastNode := d.tail.data

	d.Reverse()

	if d.head.data != lastNode {
		t.Errorf("Expected head to be %v, but got %v", lastNode, d.head.data)
	}

	if d.tail.data != firstNode {
		t.Errorf("Expected head to be %v, but got %v", firstNode, d.head.data)
	}

}
