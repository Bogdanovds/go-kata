package main

import "fmt"

type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

type RealAirConditioner struct {
	isOn        bool
	temperature int
}

func (ac *RealAirConditioner) TurnOn() {
	ac.isOn = true
	fmt.Println("Air conditioner turned on")
}

func (ac *RealAirConditioner) TurnOff() {
	ac.isOn = false
	fmt.Println("Air conditioner turned off")
}

func (ac *RealAirConditioner) SetTemperature(temp int) {
	ac.temperature = temp
	fmt.Printf("Temperature set to %d\n", temp)
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (aca *AirConditionerAdapter) TurnOn() {
	aca.airConditioner.TurnOn()
}

func (aca *AirConditionerAdapter) TurnOff() {
	aca.airConditioner.TurnOff()
}

func (aca *AirConditionerAdapter) SetTemperature(temp int) {
	aca.airConditioner.SetTemperature(temp)
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func (acp *AirConditionerProxy) TurnOn() {
	if acp.authenticated {
		acp.adapter.TurnOn()
	} else {
		fmt.Println("Access denied")
	}
}

func (acp *AirConditionerProxy) TurnOff() {
	if acp.authenticated {
		acp.adapter.TurnOff()
	} else {
		fmt.Println("Access denied")
	}
}

func (acp *AirConditionerProxy) SetTemperature(temp int) {
	if acp.authenticated {
		acp.adapter.SetTemperature(temp)
	} else {
		fmt.Println("Access denied")
	}
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	ac := &RealAirConditioner{}
	adapter := &AirConditionerAdapter{
		airConditioner: ac,
	}
	return &AirConditionerProxy{
		adapter:       adapter,
		authenticated: authenticated,
	}
}

func main() {
	authorized := NewAirConditionerProxy(true)
	unauthorized := NewAirConditionerProxy(false)

	authorized.TurnOn()
	authorized.SetTemperature(25)
	authorized.TurnOff()

	unauthorized.TurnOn()
	unauthorized.SetTemperature(20)
	unauthorized.TurnOff()
}
