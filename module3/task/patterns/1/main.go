package main

import "fmt"

type Order struct {
	Name     string
	Price    float64
	Quantity int
}

type PricingStrategy interface {
	Calculate(Order) float64
}

type RegularPricing struct {
}

type SalePricing struct {
	DiscountPercentage float64
	StrategyName       string
}

func (rp RegularPricing) Calculate(order Order) float64 {
	return order.Price
}

func (sp SalePricing) Calculate(order Order) float64 {
	discountedPrice := order.Price * (1 - sp.DiscountPercentage/100)
	return discountedPrice
}

func main() {
	orders := []Order{
		{"Product 1", 100.0, 3},
		{"Product 2", 200.0, 1},
	}

	pricingStrategies := []PricingStrategy{
		RegularPricing{},
		SalePricing{5.0, "Spring Sale"},
		SalePricing{10.0, "Summer Sale"},
		SalePricing{15.0, "Christmas Sale"},
	}

	for _, pricingStrategy := range pricingStrategies {
		fmt.Println("---", pricingStrategy, "---")
		var total float64
		for _, order := range orders {
			price := pricingStrategy.Calculate(order)
			fmt.Printf("%s: %.2f\n", order.Name, price)
			total += price
		}
		fmt.Printf("Total: %.2f\n", total)
	}
}
