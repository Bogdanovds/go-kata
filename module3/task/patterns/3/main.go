package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

type WeatherAPI interface {
	GetTemperature(location string) (int, error)
	GetHumidity(location string) (int, error)
	GetWindSpeed(location string) (int, error)
}

type OpenWeatherAPI struct {
	apiKey string
	client *http.Client
}

func (o *OpenWeatherAPI) GetTemperature(data string) (int, error) {
	type Fact struct {
		Temp float64 `json:"temp"`
	}
	var facts Fact
	if err := json.Unmarshal([]byte(data), &facts); err != nil {
		panic(err)
	}

	return int(facts.Temp), nil
}

func (o *OpenWeatherAPI) GetHumidity(data string) (int, error) {

	type Fact struct {
		Humidity float64 `json:"humidity"`
	}
	var facts Fact
	if err := json.Unmarshal([]byte(data), &facts); err != nil {
		panic(err)
	}

	return int(facts.Humidity), nil

}

func (o *OpenWeatherAPI) GetWindSpeed(data string) (int, error) {

	type Fact struct {
		WindSpeed float64 `json:"wind_speed"`
	}
	var facts Fact
	if err := json.Unmarshal([]byte(data), &facts); err != nil {
		panic(err)
	}

	return int(facts.WindSpeed), nil

}

type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func getTempFact(location string) (string, error) {
	apiKey := "b53513ea-e0ea-49d2-bd09-81659747eb5b"
	lat := 0.0
	lon := 0.0
	if location == "Moscow" {
		lat = 55.75396
		lon = 37.620393
	}
	if location == "St. Petersburg" {
		lat = 59.9386300
		lon = 30.3141300
	}

	url := fmt.Sprintf("https://api.weather.yandex.ru/v2/forecast?lat=%f&lon=%f&limit=1&hours=false&extra=false", lat, lon)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return "", fmt.Errorf("ошибка при создании запроса: %v", err)
	}
	req.Header.Set("X-Yandex-API-Key", apiKey)
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return "", fmt.Errorf("ошибка при выполнении запроса: %v", err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("ошибка при чтении ответа: %v", err)
	}

	var data map[string]interface{}
	if err := json.Unmarshal(body, &data); err != nil {
		panic(err)
	}

	fact := data["fact"]
	factJSON, err := json.Marshal(fact)
	if err != nil {
		panic(err)
	}

	return string(factJSON), nil
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int, error) {
	data, _ := getTempFact(location)

	temperature, err := w.weatherAPI.GetTemperature(data)
	if err != nil {
		return 0, 0, 0, err
	}

	humidity, err := w.weatherAPI.GetHumidity(data)
	if err != nil {
		return 0, 0, 0, err
	}

	windSpeed, err := w.weatherAPI.GetWindSpeed(data)
	if err != nil {
		return 0, 0, 0, err
	}

	return temperature, humidity, windSpeed, nil
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{
			apiKey: apiKey,
			client: &http.Client{
				Timeout: time.Second * 10,
			},
		},
	}
}

func getWeatherInfo(weatherFacade WeatherFacade, city string) {
	temperature, humidity, windSpeed, err := weatherFacade.GetWeatherInfo(city)
	if err != nil {
		fmt.Printf("Error getting weather info for %s: %v\n", city, err)
		return
	}

	fmt.Printf("Temperature in %s: %d\n", city, temperature)
	fmt.Printf("Humidity in %s: %d\n", city, humidity)
	fmt.Printf("Wind speed in %s: %d\n\n", city, windSpeed)
}

func main() {
	weatherFacade := NewWeatherFacade("b53513ea-e0ea-49d2-bd09-81659747eb5b")
	cities := []string{"Moscow", "St. Petersburg"}

	for _, city := range cities {
		getWeatherInfo(weatherFacade, city)
	}
}
