package main

import (
	"math/rand"
	"testing"
)

func Benchmark_LoadData(b *testing.B) {
	d := &DoubleLinkedList{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		if err := d.LoadData("test.json"); err != nil {
			b.Fatalf("Failed to load data: %s", err)
		}
	}
}

func Benchmark_Search(b *testing.B) {
	commits := GenerateJSON(10000)
	d := &DoubleLinkedList{}
	for _, commit := range commits {
		_ = d.Insert(d.Len(), commit)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = d.Search(commits[rand.Intn(len(commits))].Message)
	}
}

func Benchmark_SearchUUID(b *testing.B) {
	commits := GenerateJSON(10000)
	d := &DoubleLinkedList{}
	for _, commit := range commits {
		_ = d.Insert(d.Len(), commit)
	}
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		idx := rand.Intn(len(commits))
		uuid := commits[idx].UUID
		d.SearchUUID(uuid)
	}
}

func Benchmark_Reverse(b *testing.B) {
	d, err := GenData(10000)
	if err != nil {
		b.Fatalf("failed to genData: %v", err)
	}
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		d.Reverse()
	}
}

func Benchmark_Shift(b *testing.B) {
	d, err := GenData(10000)
	if err != nil {
		b.Fatalf("failed to genData: %v", err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Shift()
	}
}

func BenchmarkPop(b *testing.B) {
	d, err := GenData(10000)
	if err != nil {
		b.Fatalf("failed to genData: %v", err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Pop()
	}
}

func Benchmark_Len(b *testing.B) {
	d, err := GenData(10000)
	if err != nil {
		b.Fatalf("failed to genData: %v", err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Len()
	}
}

func Benchmark_Current(b *testing.B) {
	d, err := GenData(10000)
	if err != nil {
		b.Fatalf("failed to genData: %v", err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Current()
	}
}

func Benchmark_Prev(b *testing.B) {
	d, err := GenData(10000)
	if err != nil {
		b.Fatalf("failed to genData: %v", err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Prev()
	}
}

func Benchmark_Next(b *testing.B) {
	d, err := GenData(10000)
	if err != nil {
		b.Fatalf("failed to genData: %v", err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		d.Next()
	}
}

func Benchmark_Insert(b *testing.B) {
	commits := GenerateJSON(1000)
	d := &DoubleLinkedList{}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = d.Insert(i, commits[rand.Intn(len(commits))])
		d.len++
	}
}

func BenchmarkDelete(b *testing.B) {
	d, err := GenData(10000)
	if err != nil {
		b.Fatalf("failed to genData: %v", err)
	}
	b.ResetTimer()
	for i := 0; i < 10000; i++ {
		n := rand.Intn(d.len)
		if err := d.Delete(n); err != nil {
			b.Fatal(err)
		}
	}
}

func Benchmark_DeleteCurrent(b *testing.B) {
	d, err := GenData(10000)
	if err != nil {
		b.Fatalf("failed to genData: %v", err)
	}
	b.ResetTimer()
	for i := 0; i < 10000; i++ {
		err := d.DeleteCurrent()
		if err != nil {
			b.Fatalf("failed to delete current node: %v", err)
		}
	}
}

func Benchmark_Index(b *testing.B) {
	d, err := GenData(10000)
	if err != nil {
		b.Fatalf("failed to genData: %v", err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, err := d.Index()
		if err != nil {
			b.Fatal(err)
		}
	}
}
