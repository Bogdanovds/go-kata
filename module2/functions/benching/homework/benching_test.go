package main

import (
	"testing"
)

var (
	users    = genUsers()
	products = genProducts()
)

func BenchmarkMapUserProducts(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts(users, products)
	}
}

func BenchmarkMapUserProducts2(b *testing.B) {
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}

}
