package main

import (
	"errors"
	"fmt"
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	job.IsFinished = true
	job.Merged = make(map[string]string, len(job.Dicts))
	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}
	for i := 0; i < len(job.Dicts); i++ {
		if job.Dicts[i] == nil {
			return job, errNilDict
		} else {
			for key, value := range job.Dicts[i] {
				job.Merged[key] = value
			}
		}

	}
	return job, errors.New("nil")
}
func main() {
	m, err := ExecuteMergeDictsJob(&MergeDictsJob{})
	if m.Merged == nil {
		fmt.Printf("&MergeDictsJob{IsFinished: %v, %q \n", m.IsFinished, err)
	}
	fmt.Printf("&MergeDictsJob{IsFinished: %v, Dicts: []%#v, %q \n", m.IsFinished, m.Merged, err)
}

// Пример работы
// ExecuteMergeDictsJob(&MergeDictsJob{}) // &MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil
