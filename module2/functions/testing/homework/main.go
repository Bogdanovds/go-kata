package main

import (
	"fmt"
	"unicode"
)

func Greet(name string) string {
	var fL, tL, fC, tC int
	r := []rune(name)
	for i := range r {
		if unicode.Is(unicode.Cyrillic, r[i]) == false {
			fC++
		} else {
			tC++
		}
		if unicode.Is(unicode.Latin, r[i]) == false {
			fL++
		} else {
			tL++
		}
	}
	if tC > 0 && fC == 0 {
		return fmt.Sprintf("Привет %s, добро пожаловать!", name)
	} else if tL > 0 && fL == 0 {
		return fmt.Sprintf("Hello %s, you welcome!", name)
	} else {
		return "Ошибка ввода"
	}
}
