package main

import (
	"context"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const timeLimit = 3 * time.Second

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	ticker := time.NewTicker(timeLimit)

	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	ctx := context.Background()
	ctx, cc := context.WithCancel(ctx)

	go func(ctx context.Context) {
		for {
			select {
			case a <- <-out:
			case <-ctx.Done():
			default:
				break
			}
		}
	}(ctx)

	go func(ctx context.Context) {
		for {
			select {
			case b <- <-out:
			case <-ctx.Done():
			default:
				break
			}
		}
	}(ctx)

	go func(ctx context.Context) {
		for {
			select {
			case c <- <-out:
			case <-ctx.Done():
			default:
				break
			}
		}
	}(ctx)

	defer func() {
		<-ticker.C
		cc()

	}()

	go func() {
		mainChan := joinChannels(a, b, c)

		for num := range mainChan {
			fmt.Println(num)
		}

	}()

}
