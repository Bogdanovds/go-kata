package main

import (
	"fmt"
	"sync"
)

type Cache struct {
	data  map[string]interface{}
	mutex sync.Mutex
	init  bool
}

func NewCache() *Cache {
	return &Cache{
		data: make(map[string]interface{}, 100),
		init: true,
	}
}

func (c *Cache) Set(key string, v interface{}) error {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	if !c.init {
		return fmt.Errorf("cache isnt initialized")
	}

	c.data[key] = v

	return nil
}

func (c *Cache) Get(key string) interface{} {
	if !c.init {
		return nil
	}
	return c.data[key]
}

func main() {
	cache := NewCache()
	var wg sync.WaitGroup

	keys := []string{
		"programming",
		"is",
		"so",
		"awesome",
		"write",
		"clean",
		"code",
		"use",
		"solid",
		"principles",
	}

	wg.Add(len(keys))
	for i := range keys {
		idx := i
		go func() {
			err := cache.Set(keys[idx], idx)
			if err != nil {
				panic(err)
			}
			wg.Done()
		}()
	}
	wg.Wait()

	fmt.Println(cache.data)
}
