package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(nil)
}

func test(r interface{}) {
	switch r.(type) {
	case nil:
		fmt.Println("Success!")
	}

}
