/*
Создайте интерфейс Transporter, который содержит метод Transport() для перемещения объекта в место назначения.
Создайте структуру Car, которая содержит поле название и метод Transport(), который выводит сообщение о
перемещении машины в место назначения. Создайте структуру Plane, которая также содержит поле название и
метод Transport(), который выводит сообщение о перемещении самолета в место назначения.
Используйте мапу для хранения списка транспортных средств.
Напишите функцию для пермещения всех транспортных средств в место назначения.
*/
package main

import "fmt"

type Transporter interface {
	transport() string
}
type Car struct {
	Name string
	Transporter
}
type Plane struct {
	Name string
	Transporter
}

func (vehicle Car) transport() string {
	return vehicle.Name + " --Приехал"
}

func (vehicle Plane) transport() string {
	return vehicle.Name + " --Прилетел"
}

func main() {
	a := Car{Name: "Автомобиль"}
	b := Plane{Name: "Самолёт"}
	fmt.Println(Car.transport(a), Plane.transport(b))
}
