/*
Создайте интерфейс Fighter, который содержит методы Attack() и Defend().
Создайте структуру Knight, которая реализует интерфейс Fighter и содержит поле силы атаки.
Создайте структуру Mage, которая также реализует интерфейс Fighter и содержит поле силы магии.
Создайте функцию, которая принимает объект типа Fighter и вызывает его методы Attack() и Defend().
*/
package main

import "fmt"

type Fighter interface {
	Attack() int
	Defend() int
}
type Knight struct {
	Fighter
	physical_damage int
}
type Mage struct {
	Fighter
	magical_damage int
}

func action(a Fighter) {
	a.Attack()

	//a.Defend()
}
func (a Knight) Attack() int {
	c := a.physical_damage
	return c
}
func main() {
	fighter := Knight{physical_damage: 50}

	fmt.Println(fighter)
}
