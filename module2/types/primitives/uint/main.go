package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")
	typeUint()
	typeInt()
}
func typeInt() {
	fmt.Println("=== START type uint ===")
	var numberInt8 int8 = 1 << 1
	fmt.Println("left shift int8:", numberInt8, "size:", unsafe.Sizeof(numberInt8), "bytes")
	numberInt8 = (1 << 7) - 1
	fmt.Println("int8 max value:", numberInt8, "size:", unsafe.Sizeof(numberInt8), "bytes")
	var numberInt16 int16 = (1 << 15) - 1
	fmt.Println("int16 max value:", numberInt16, "size:", unsafe.Sizeof(numberInt16), "bytes")
	var numberInt32 int32 = (1 << 31) - 1
	fmt.Println("int32 max value:", numberInt32, "size:", unsafe.Sizeof(numberInt32), "bytes")
	var numberInt64 int64 = (1 << 63) - 1
	fmt.Println("int64 max value:", numberInt64, "size:", unsafe.Sizeof(numberInt64), "bytes")
	fmt.Println("=== END type uint ===")
}
func typeUint() {
	fmt.Println("=== START type uint ===")
	var numberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8:", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")
	numberUint8 = (1 << 8) - 1
	fmt.Println("uint8 max value:", numberUint8, "size:", unsafe.Sizeof(numberUint8), "bytes")
	var numberUint16 uint16 = (1 << 16) - 1
	fmt.Println("uint16 max value:", numberUint16, "size:", unsafe.Sizeof(numberUint16), "bytes")
	var numberUint32 uint32 = (1 << 32) - 1
	fmt.Println("uint32 max value:", numberUint32, "size:", unsafe.Sizeof(numberUint32), "bytes")
	var numberUint64 uint64 = (1 << 64) - 1
	fmt.Println("uint64 max value:", numberUint64, "size:", unsafe.Sizeof(numberUint64), "bytes")
	fmt.Println("=== END type uint ===")
}
