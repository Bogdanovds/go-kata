//Напишите функцию, которая удаляет дубликаты из слайса строк.
package main

import "fmt"

func keepOne(a []string) []string {
	keepOne := make([]string, 0)
	for i := 0; i < len(a); i++ {
		if elementExists(keepOne, a[i]) == false {
			keepOne = append(keepOne, a[i])
		}
	}
	return keepOne
}
func elementExists(haystack []string, needle string) bool {
	for _, v := range haystack {
		if v == needle {
			return true
		}
	}
	return false
}

func main() {
	var a = []string{"1", "3", "3", "1", "2", "9", "18", "3", "5", "2", "7"}
	fmt.Println(a)
	fmt.Println(keepOne(a))
}
