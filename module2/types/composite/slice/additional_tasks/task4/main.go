//Напишите функцию, которая объединяет два слайса и удаляет дубликаты
package main

import "fmt"

func main() {
	var a = []int{3, 11, 19, 36, 22, 92, 18, 4, 5, 22, 7}
	var b = []int{11, 3, 19, 36, 22, 92, 18, 4, 5, 2, 71}
	fmt.Println(unionData(a, b))
	fmt.Println(keepOne(unionData(a, b)))

}
func unionData(a, b []int) []int {
	unionSlice := make([]int, len(a))
	copy(unionSlice, a)
	unionSlice = append(unionSlice, b...)
	return unionSlice
}
func keepOne(a []int) []int {
	keepOne := make([]int, 0)
	for i := 0; i < len(a); i++ {
		if elementExists(keepOne, a[i]) == false {
			keepOne = append(keepOne, a[i])
		}
	}
	return keepOne
}
func elementExists(haystack []int, needle int) bool {
	for _, v := range haystack {
		if v == needle {
			return true
		}
	}
	return false
}
