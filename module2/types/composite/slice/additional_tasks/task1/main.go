//Напишите функцию, которая находит максимальный элемент в слайсе целых чисел.
package main

import "fmt"

func maxValue(a []int) int {
	var b int = 0
	for i := range a {
		if a[i] > b {
			b = a[i]
		}
	}
	return b
}

func main() {
	var a = []int{1, 3, 19, 6, 2, 9, 18, 4, 5, 2, 7}
	fmt.Println(maxValue(a))
}
