package main

import "fmt"

func main() {
	s := []int{1, 2, 3}
	s = Append(s)
	fmt.Println(s)
}

func Append(s []int) []int {
	n := len(s)
	if n == cap(s) {
		// Slice is full; must grow.
		// We double its size and add 1, so if the size is zero we still grow.
		newSlice := make([]int, len(s), 2*len(s)+1)
		copy(newSlice, s)
		s = newSlice
	}
	s = s[0 : n+1]
	s[n] = 4
	return s
}
