package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		// сюда впишите ваши остальные 12 структур
		{
			Name:  "https://github.com/golang/go",
			Stars: 110000,
		},
		{
			Name:  "https://github.com/avelino/awesome-go",
			Stars: 98900,
		},
		{
			Name:  "https://github.com/gin-gonic/gin",
			Stars: 67700,
		},
		{
			Name:  "https://github.com/caddyserver/caddy",
			Stars: 46500,
		},
		{
			Name:  "https://github.com/traefik/traefik",
			Stars: 42300,
		},
		{
			Name:  "https://github.com/v2ray/v2ray-core",
			Stars: 42000,
		},
		{
			Name:  "https://github.com/astaxie/build-web-application-with-golang",
			Stars: 41800,
		},
		{
			Name:  "https://github.com/Dreamacro/clash",
			Stars: 31900,
		},
		{
			Name:  "https://github.com/rclone/rclone",
			Stars: 37700,
		},
		{
			Name:  "https://github.com/nektos/act",
			Stars: 36500,
		},
		{
			Name:  "https://github.com/go-gitea/gitea",
			Stars: 35600,
		},
		{
			Name:  "https://github.com/go-gorm/gorm",
			Stars: 31900,
		},
	}
	projectsMap := make(map[string]int)
	for i := 0; i < len(projects); i++ {
		projectsMap[projects[i].Name] = projects[i].Stars
	}
	for k, v := range projectsMap {
		fmt.Println(k, v)
	}
}
