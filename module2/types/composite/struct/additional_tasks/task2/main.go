//Создайте структуру Student, которая содержит поля имя, возраст и список курсов, которые студент проходит.
//Каждый курс представляется в виде структуры Course, которая содержит поля название курса, описание и оценку.
//Используйте мапу для хранения списка студентов.
//Напишите функцию для вывода всех студентов и их курсов с оценками.
package main

import "fmt"

type Student struct {
	Name     string
	Age      int
	Courses  Courses
	Location Location
}
type Courses struct {
	Title       string
	Description string
	Grade       int
}
type Location struct {
	City string
}

func main() {
	c := []Courses{
		{
			Title:       "math",
			Description: "algebra",
			Grade:       4,
		},
		{
			Title:       "rus",
			Description: "russian language",
			Grade:       5,
		},
		{
			Title:       "chem",
			Description: "chemistry",
			Grade:       3,
		},
	}
	s := []Student{
		{
			Name: "Bogdan",
			Age:  24,
			Location: Location{
				City: "spb",
			},
		},
		{
			Name: "Ivan",
			Age:  23,
			Location: Location{
				City: "spb",
			},
		},
		{
			Name: "Fedor",
			Age:  21,
			Location: Location{
				City: "spb",
			},
		},
	}
	studentMap := make(map[string]string)
	for i := 0; i < len(s); i++ {
		studentMap[s[i].Name] = c[i].Title
	}

	fmt.Println(c)
	fmt.Println(s)
	fmt.Println(studentMap)
}
