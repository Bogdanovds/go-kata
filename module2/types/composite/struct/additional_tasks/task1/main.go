//Напишите структуру Student, которая содержит поля name, age и grade.
//Напишите метод, который печатает информацию о студенте в формате "Имя: {name}, Возраст: {age}, Класс: {grade}".
package main

import "fmt"

type Student struct {
	name  string
	age   int
	grade int
}

func (s Student) info() Student {
	fmt.Println("Имя:", s.name, "Возраст:", s.age, "Класс:", s.grade)
	return s
}
func main() {
	s := Student{"Alex", 22, 3}
	s.info()
}
