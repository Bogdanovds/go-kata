// Создайте структуру animal, которая содержит поля имя, вид и список характеристик.
// Каждая характеристика представляется в виде структуры Characteristic, которая содержит поле название и значение.
// Используйте мапу для хранения списка животных.
// Напишите функцию для вывода всех животных и их характеристик.
package main

import "fmt"

type animal struct {
	Name           string
	Type           string
	Characteristic Characteristic
}
type Characteristic struct {
	weight int
	speed  int
}

func main() {
	animals := []animal{
		{
			Name: "Лев",
			Type: "кот",
			Characteristic: Characteristic{
				weight: 10,
				speed:  20,
			},
		},
		{
			Name: "Слон",
			Type: "слон",
			Characteristic: Characteristic{
				weight: 100,
				speed:  2,
			},
		},
	}
	//fmt.Println(animals)
	listAnimals(animals)
}
func listAnimals(animals []animal) {
	animalsMap := make(map[string]Characteristic)
	for _, v := range animals {
		animalsMap[v.Name] = v.Characteristic
	}
	fmt.Println(animalsMap)
}
