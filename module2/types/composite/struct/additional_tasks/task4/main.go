package main

import (
	"fmt"
	"sort"
)

type Employee struct {
	Name       string
	Age        int
	Salary     float64
	Department string
}

type Sortable interface {
	Len() int
	Less(i, j int) bool
	Swap(i, j int)
}

type ByAge []Employee

func (a ByAge) Len() int           { return len(a) }
func (a ByAge) Less(i, j int) bool { return a[i].Age < a[j].Age }
func (a ByAge) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

type BySalary []Employee

func (a BySalary) Len() int           { return len(a) }
func (a BySalary) Less(i, j int) bool { return int(a[i].Salary) < int(a[j].Salary) }
func (a BySalary) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

type ByDepartment []Employee

func (a ByDepartment) Len() int           { return len(a) }
func (a ByDepartment) Less(i, j int) bool { return a[i].Department < a[j].Department }
func (a ByDepartment) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

func main() {
	// employees := generateRandomEmployees(10)
	employees := []Employee{
		{
			Name:       "John",
			Age:        25,
			Salary:     125.8,
			Department: "Mks",
		},
		{
			Name:       "Kate",
			Age:        21,
			Salary:     155.34,
			Department: "Abs",
		},
		{
			Name:       "Elena",
			Age:        19,
			Salary:     1324.8,
			Department: "Sgs",
		},
	}
	for {
		printMenu()
		var choice int
		fmt.Scan(&choice)

		switch choice {
		case 1:
			printEmployees(employees)
		case 2:
			sort.Sort(ByAge(employees))
			printEmployees(employees)
		case 3:
			sort.Sort(BySalary(employees))
			printEmployees(employees)
		case 4:
			sort.Sort(ByDepartment(employees))
			printEmployees(employees)
		case 5:
			fmt.Println("Goodbye!")
			return
		default:
			fmt.Println("Invalid choice!")
		}
	}
}

// func generateRandomEmployees(n int) []Employee {
// 	// сгенерировать рандомных работников

// }

func printMenu() {
	fmt.Println("Please choose an option:")
	fmt.Println("1. Print all employees")
	fmt.Println("2. Sort employees by age")
	fmt.Println("3. Sort employees by salary")
	fmt.Println("4. Sort employees by department")
	fmt.Println("5. Exit")
}

func printEmployees(employees []Employee) {
	// вывести работников
	fmt.Println(employees)
}
