module gitlab.com/Bogdanovds/example

go 1.18

replace gitlab.com/Bogdanovds/greet => ../greet

require gitlab.com/Bogdanovds/greet v0.0.0-20230406075001-89ac3c806c27 // indirect
