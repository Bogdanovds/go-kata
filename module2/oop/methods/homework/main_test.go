package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMultiply(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 1.0, b: 1.0, expected: 1.0},
		{a: 1.0, b: 2.0, expected: 2.0},
		{a: 1.0, b: 3.0, expected: 3.0},
		{a: 1.0, b: 4.0, expected: 4.0},
		{a: 1.0, b: 5.0, expected: 5.0},
		{a: 1.0, b: 6.0, expected: 6.0},
		{a: 1.0, b: 7.0, expected: 7.0},
		{a: 1.0, b: 8.0, expected: 8.0},
	}

	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := multiply(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)

		fmt.Printf("testMultiply : a = %g, b = %g while expected result equals to %g, "+"received result equals to %g\n", test.a, test.b, test.expected, receivedResult)
	}
}

func TestDivide(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 1.0, b: 1.0, expected: 1.0},
		{a: 1.0, b: 2.0, expected: 0.5},
		{a: 1.0, b: 3.0, expected: 0.3333333333333333},
		{a: 1.0, b: 4.0, expected: 0.25},
		{a: 1.0, b: 5.0, expected: 0.2},
		{a: 1.0, b: 6.0, expected: 0.16666666666666666},
		{a: 1.0, b: 7.0, expected: 0.14285714285714285},
		{a: 1.0, b: 8.0, expected: 0.125},
	}

	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := divide(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)

		fmt.Printf("testDivide : a = %g, b = %g while expected result equals to %g, "+"received result equals to %g\n", test.a, test.b, test.expected, receivedResult)
	}
}

func TestSum(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 1.0, b: 1.0, expected: 2.0},
		{a: 1.0, b: 2.0, expected: 3.0},
		{a: 1.0, b: 3.0, expected: 4.0},
		{a: 1.0, b: 4.0, expected: 5.0},
		{a: 1.0, b: 5.0, expected: 6.0},
		{a: 1.0, b: 6.0, expected: 7.0},
		{a: 1.0, b: 7.0, expected: 8.0},
		{a: 1.0, b: 8.0, expected: 9.0},
	}

	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := sum(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)

		fmt.Printf("testSum : a = %g, b = %g while expected result equals to %g, "+"received result equals to %g\n", test.a, test.b, test.expected, receivedResult)
	}
}

func TestAverage(t *testing.T) {
	type testCase struct {
		a        float64
		b        float64
		expected float64
	}

	testCases := []testCase{
		{a: 1.0, b: 1.0, expected: 1.0},
		{a: 1.0, b: 2.0, expected: 1.5},
		{a: 1.0, b: 3.0, expected: 2.0},
		{a: 1.0, b: 4.0, expected: 2.5},
		{a: 1.0, b: 5.0, expected: 3.0},
		{a: 1.0, b: 6.0, expected: 3.5},
		{a: 1.0, b: 7.0, expected: 4.0},
		{a: 1.0, b: 8.0, expected: 4.5},
	}

	for _, test := range testCases {
		expectedResult := test.expected
		receivedResult := average(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)

		fmt.Printf("testAverage : a = %g, b = %g while expected result equals to %g, "+"received result equals to %g\n", test.a, test.b, test.expected, receivedResult)
	}
}
