package main

import (
	"os"

	"github.com/essentialkaos/translit"
)

func main() {

	file, err := os.Open("example.txt")
	check(err)

	fInfo, err := file.Stat()
	check(err)

	data := make([]byte, fInfo.Size())
	_, err = file.Read(data)
	check(err)

	check(file.Close())

	file, err = os.Create("example.processed.txt")
	check(err)
	data1 := string(data)
	dataTrans := translit.EncodeToISO9A(data1)

	_, err = file.WriteString(dataTrans)
	check(err)

	check(file.Sync())
	check(file.Close())
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
