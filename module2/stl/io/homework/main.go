package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	var buf bytes.Buffer

	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}

	for _, text := range data {
		_, err := buf.Write([]byte(text))
		check(err)
		_, err = buf.Write([]byte("\n"))
		check(err)
	}

	file, err := os.Create("example.txt")
	check(err)

	_, err = file.Write(buf.Bytes())
	check(err)
	check(file.Close())

	file, err = os.Open("example.txt")
	check(err)
	var buf2 bytes.Buffer
	_, err = io.Copy(&buf2, file)
	check(err)

	check(file.Close())

	fmt.Println(buf2.String())
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
