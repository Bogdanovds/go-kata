package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string
	Production bool
}

func main() {
	var patch string
	flag.StringVar(&patch, "conf", "module2/stl/flags/homework/config.json", "Patch to config")
	flag.Parse()

	file, err := os.Open(patch)
	check(err)

	fileInfo, err := file.Stat()
	check(err)

	byteConf := make([]byte, fileInfo.Size())
	_, err = file.Read(byteConf)
	check(err)

	var conf Config
	err = json.Unmarshal(byteConf, &conf)
	check(err)

	fmt.Printf("%#v", conf)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
