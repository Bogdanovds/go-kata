package main

import (
	"encoding/json"

	jsoniter "github.com/json-iterator/go"
)

type Welcome []WelcomeElement

func UnmarshalWelcome(data []byte) (Welcome, error) {
	var r Welcome
	err := json.Unmarshal(data, &r)
	return r, err
}

func UnmarshalWelcome2(data []byte) (Welcome, error) {
	var r Welcome
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Welcome) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func (r *Welcome) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type WelcomeElement struct {
	ID        int64      `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}
